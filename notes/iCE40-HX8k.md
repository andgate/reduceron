# iCE40 HX8K FPGA

This project will use an iCE40 HX8K CT256 FPGA.

LEs 7680
RAM 128 kbit or 16Kb
PPLs 2
Static Current 1140 μA
Max I/Os 208

The FPGA is a iCE40HX-8K in a 256-pin LFBGA

LFBGA: Low-profile Fine-pitch Ball Grid Array

Pin locations are labeled on the board.

Built-in LEDs are located on pins D2 to D9

Has a FTDI 2232H for USB interface.
Write access can be configured on linux via:
```
cat - <<EOF > /etc/udev/rules.d/53-lattice-ftdi.rules
ACTION=="add", ATTR{idVendor}=="0403", ATTR{idProduct}=="6010", MODE:="666"
EOF
```
This edits udev rules, which may not be possible with nix on windows?


12MHz oscillator at pin J3, with 2 PLLs to scale the incoming clock.
We can get a 96MHz system clock with some magic numbers

```
$ icepll -i 12 -o 96 -m -f pll.v	
					
F_PLLIN:    12.000 MHz (given)		
F_PLLOUT:   96.000 MHz (requested)	
F_PLLOUT:   96.000 MHz (achieved)	
					
FEEDBACK: SIMPLE			
F_PFD:   12.000 MHz			
F_VCO:  768.000 MHz			
					
DIVR:  0 (4'b0000)			
DIVF: 63 (7'b1000010)			
DIVQ:  3 (3'b011)			
					
FILTER_RANGE: 1 (3'b001)		
					
PLL configuration written to: pll.v	
```

`icepll` can generate this clock exactly and will write it to a verilog file. 
`icepll` can scale between 16MHz and 275MHz. This essentially lets us "overclock" our reduceron and test it on different frequencies.
The generated PLL doesn't use global clock buffers, so it needs to be tweaked by hand.

WSL does not support usb devices, so flashing can only be done from linux.

Using older clash with ICE40 PLL
 - https://gist.github.com/thoughtpolice/8ec923e1b3fc4bb12c11aa23b4dc53b5
 - https://github.com/thoughtpolice/clash-playground/blob/d5215da20f698e8c423334f99e725ecf1ce7e4b7/src/Lattice.hs

Current Clash has removed clock source annotations.
Instead, PLLs can be instantiated in Haskell and implemented using HDL.
http://hackage.haskell.org/package/clash-prelude-1.0.0/docs/Clash-Annotations-Primitive.html


The reduceron built on an Xilinx Virtex-5 FPGA has the following memory devices:

```
  +------------------+----------+------------------+----------+
  |   Memory Unit    | Element  | Bits per Element | Elements |
  +------------------+----------+------------------+----------+
  | Program          | Template |              234 | 1k       |
  | Heap             | App      |               77 | 8k       |
  | Reduction stack  | Atom     |               18 | 4k       |
  | Update stack     | Update   |               28 | 4k       |
  | Case-table stack | Atom     |               18 | 4k       |
  | Copy space       | App      |               77 | 16k      |
  +------------------+----------+------------------+----------+
  
  Total required kbits: 2410 kbits
  Available on iCE40:   128 kbits
```

Obviously this won't work on our iCE40. The number of elements must be heavily reduced
to fit onto the chip.

It will be important have parameters for `system` to configure memory devices