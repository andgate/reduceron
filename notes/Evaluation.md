# Understanding Template Instantiation Evaluators

## The Evaluation Process
  1) select the outmost redex
  2) reduce it
  3) update the (root of the) redex with the result
  4) Repeat until there are no more redexes (normal-form)


## Unwinding the spine
  - First step in the reduction cycle
  - Finds the outermost reducible function application.
  - The spine is a chain of applications along the
    leftside of the expression tree.
  - With partial application, then step 2 needs to be
    proceeded by a check that there are enough application 
    nodes in the spine. If not, then the expression is WHNF.
  - WHNF check can be omitted if the program is typechecked.

### Steps
  1) Start at the root
  2) Follow the left branch until you get to a super combinator
      or built-in primitive.
  3) Find the number of arguments the supercombinator/built-in takes.
  4) Go back that number of nodes.
  5) The node you land on is root of the outermost function application.

## Updates
  - Update the root of the redex with the result of a           reduction
  - If the redex is shared, this gaurantees the reduction is    done once.
  - This update is the essence of lazy evaluation


## Constant Applicative Form (CAF)
  - Supercombinators which have no arguments
  - The supercombinator itself is a redex
  - To avoid repeated reductions, supercombinators should be
    represented by graph nodes.


## Graph Reduction Machine State

The state of the template instantiation graph reduction machine is a quadruple
```
(stack, dump, heap, globals)
```
where
  - *stack*: A stack of addresses, each of which identifies a
    node in the heap. These nodes form the spine of the
    expression being evaluated.
  - *dump*: the records of the spine stack prior to the
    evaluation of an argument of a strict primitive.
  - *heap*: A collection of tagged nodes.
  - *globals*: Contains the addresses of heap nodes
    representing each supercombinator (or primitive).


## Applying a supercombinator (instantiation)

To apply a supercombinator, we must *instantiate* its body by

  1) Bind argument names to addresses found in the stack
  2) Discard the arguments from the stack (including the root
     of the redex)
  3) Push the (root of the) result of the reduction onto the
     stack
  4) Update the root of the redex with the reuslt of the 
     reduction?




