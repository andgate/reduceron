## Unsolved Problems

1. Caching/Large External Memory Storage
2. Parallel Evaluation
3. Operating system
    - Loading/Scheduling Programs (Memory manupulations)
    - Interupts
    - Interfacing effectful hardware (HDD, GPU, Keyboard, Mouse, Screen, USB, etc.)
4.  Well-behaved effectful computations (IO monad)