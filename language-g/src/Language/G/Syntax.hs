{-# LANGUAGE TypeOperators
           , TemplateHaskell
           , GADTs
           , TypeFamilies
           , DataKinds
           , ConstraintKinds
           , LambdaCase
           , ViewPatterns
           , StandaloneDeriving
           , MultiParamTypeClasses
           , FlexibleInstances
           , RankNTypes
           , DeriveFunctor
           , DeriveFoldable
           , DeriveTraversable
           , TemplateHaskell
           #-}
module Language.G.Syntax where

import Data.List (elemIndex)
import Data.Maybe (fromJust)

import Data.List.NonEmpty (NonEmpty, (<|))
import qualified Data.List.NonEmpty as NE
import Data.Constraint

import Bound
import Control.Applicative
import Control.Monad
import Data.Functor.Classes
import Data.Foldable
import Data.Traversable

import Language.G.Syntax.Location


------------------------------------------------------------------------
-- Language Projections
------------------------------------------------------------------------

data Lang = L1 | L2 | L3

-- Type family that returns a constructor. Tells you if a Lang
-- is in a list of Langs. 
type family IsIn (l :: Lang) (ls :: [Lang]) :: Constraint where
    IsIn l (l ': ls) = ()
    IsIn l (z ': ls) = IsIn l ls


------------------------------------------------------------------------
-- Module Syntax
------------------------------------------------------------------------

data Module l a = Module Loc String [Stmt l a]

data Stmt l a
  = FuncDefn (Func l (Expr l) a)
  | FuncSig (Sig l)
  | DataTypeDefn (DataType l)
  | ImportStmt Import


------------------------------------------------------------------------
-- Import Syntax
------------------------------------------------------------------------

data Import = Import String (Maybe [String])

---------------------------------------------------------------------------
-- Function
---------------------------------------------------------------------------

data Func (l :: Lang) (f :: * -> *) (a :: *) where
  Func :: Loc -> String -> Clause l f a -> Func l f a

instance Bound (Func l) where
  Func l n cl >>>= f = Func l n (cl >>>= f)

instance HasLocation (Func l f a) where
  locOf (Func l _ _ ) = l

---------------------------------------------------------------------------
-- Signature
---------------------------------------------------------------------------

data Sig (l :: Lang) where
  Sig :: Loc -> String -> Type l -> Sig l

instance HasLocation (Sig l) where
  locOf (Sig l _ _) = l

---------------------------------------------------------------------------
-- Datatype
---------------------------------------------------------------------------

data DataType l
  = DataType Loc String [ConstrDefn l]

data ConstrDefn l
  = ConstrDefn Loc String [Type l]
  | RecordDefn Loc String (NonEmpty (Entry l))

-- Record Entry
data Entry l
  = Entry Loc String (Type l)


instance HasLocation (DataType l) where
  locOf (DataType l _ _) = l

instance HasLocation (ConstrDefn l) where
  locOf (ConstrDefn l _ _) = l
  locOf (RecordDefn l _ _) = l

instance HasLocation (Entry l) where
  locOf (Entry l _ _) = l

------------------------------------------------------------------------
-- Literal Syntax
------------------------------------------------------------------------

data Lit
  = LBool Bool
  | LInt Integer
  | LFp Double
  | LChar Char
  | LString String
  deriving (Show)

------------------------------------------------------------------------
-- Operators
------------------------------------------------------------------------

data PrimOp
  = OpAdd
  | OpSub
  | OpMul
  | OpDiv
  | OpRem
  | OpNeg

  | OpEq
  | OpNeq
  | OpLT
  | OpGT
  | OpGE
  | OpLE

------------------------------------------------------------------------
-- Type Syntax
------------------------------------------------------------------------

type TVar = String

data Type (l :: Lang) where
  TVar :: TVar -> Type l
  TCon :: String -> [Type l] -> Type l
  TPrim :: PrimType -> Type l
  TTuple :: NonEmpty (Type l) -> Type l
  TList  :: Type l -> Type l
  TArr :: Type l -> Type l -> Type l
  TParens :: Type l -> Type l
  TLoc :: Type l -> Loc -> Type l

data PrimType
  = TInt Int
  | TUInt Int
  | TFp Int

instance HasLocation (Type l) where
  locOf = \case
    TLoc _ l -> l
    _ -> error "expected expression with location annotation"


------------------------------------------------------------------------
-- Expression Syntax
------------------------------------------------------------------------

data Expr (l :: Lang) (a :: *) where
  -- Atoms
  EVar    :: a -> Expr l a
  ECon    :: String -> Expr l a
  ELit    :: Lit -> Expr l a

  -- Special Datatypes
  EUnit   :: Expr l a
  ETuple  :: NonEmpty (Expr l a) -> Expr l a
  EList   :: [Expr l a] -> Expr l a

  -- Operators
  EPrimOp  :: PrimOp -> Expr l a

  -- Computation
  ELam1   :: (IsIn l '[ 'L2 ]) => Pat l -> Scope Int (Expr l) a -> Expr l a
  ELamN   :: (IsIn l '[ 'L1 ]) => NonEmpty (Pat l) -> Scope Int (Expr l) a -> Expr l a
  EApp    :: (IsIn l '[ 'L1, 'L2 ]) => Expr l a -> NonEmpty (Expr l a) -> Expr l a

  -- Local Bindings
  ELet1   :: LetBind l (Expr l) a -> Scope Int (Expr l) a -> Expr l a
  ELetN   :: NonEmpty (LetBind l (Expr l) a) -> Scope Int (Expr l) a -> Expr l a

  -- Controls
  EIf     :: Expr l a -> Expr l a -> Else l (Expr l) a -> Expr l a
  ECase   :: Expr l a -> NonEmpty (Alt l (Expr l) a) -> Expr l a
  
  -- Annotations
  EType   :: Expr l a -> Type l -> Expr l a
  ELoc    :: Expr l a -> Loc -> Expr l a
  EParens :: Expr l a -> Expr l a


deriving instance Functor (Expr l)
deriving instance Foldable (Expr l)
deriving instance Traversable (Expr l)

instance Applicative (Expr l) where
  pure = EVar
  (<*>) = ap

instance Monad (Expr l) where
  return = EVar
  (>>=) e f = case e of
    EVar a -> f a
    ECon c -> ECon c
    ELit l -> ELit l

    EUnit -> EUnit
    ETuple es -> ETuple (fmap (>>= f) es)
    EList  es -> EList (fmap (>>= f) es)

    EPrimOp op -> EPrimOp op 

    ELam1 p b -> ELam1 p (b >>>= f)
    ELamN ps b -> ELamN ps (b >>>= f)
    EApp x ys -> EApp (x >>= f) (fmap (>>= f) ys)

    ELet1 a e -> ELet1 (a >>>= f) (e >>>= f)
    ELetN as e -> ELetN (fmap (>>>= f) as) (e >>>= f)

    EIf p a e -> EIf (p >>= f) (a >>= f) (e >>>= f)
    ECase s brs -> ECase (s >>= f) (fmap (>>>= f) brs)

    EType e ty -> EType (e >>= f) ty
    ELoc e l -> ELoc (e >>= f) l
    EParens e -> EParens (e >>= f)


instance HasLocation (Expr l a) where
  locOf = \case
    ELoc _ l -> l
    _ -> error "expected expression with location annotation"

------------------------------------------------------------------------
-- Pattern Syntax
------------------------------------------------------------------------

data Pat (l :: Lang) where
  PVar    :: String -> Pat l
  PCon    :: String -> [Pat l] -> Pat l

  PUnit   :: Pat l
  PTuple  :: NonEmpty (Pat l) -> Pat l
  PList   :: [Pat l] -> Pat l

  PWild   :: Pat l
  PAs     :: String -> Pat l -> Pat l
  PType   :: Pat l -> Type l -> Pat l
  PLoc    :: Pat l -> Loc -> Pat l
  PParens :: Pat l -> Pat l


instance HasLocation (Pat l) where
  locOf = \case
    PLoc _ l -> l
    _ -> error "expected pattern with location"


------------------------------------------------------------------------
-- Alt Syntax
------------------------------------------------------------------------

data Alt (l :: Lang) (f :: * -> *) (a :: *) where
  Alt :: Maybe Loc -> Pat l -> Scope Int f a -> Alt l f a

deriving instance Functor f     => Functor     (Alt l f)
deriving instance Foldable f    => Foldable    (Alt l f)
deriving instance Traversable f => Traversable (Alt l f)

instance Bound (Alt l) where
  Alt l ps b >>>= f = Alt l ps (b >>>= f)

instance HasLocation (Alt l f a) where
  locOf (Alt l _ _) = maybe (error "Alt missing source annotation") id l

------------------------------------------------------------------------
-- Let Bind Syntax
------------------------------------------------------------------------

data LetBind (l :: Lang) (f :: * -> *) (a :: *) where
  LetBind :: Maybe Loc -> Pat l -> Scope Int f a -> LetBind l f a

deriving instance Functor f     => Functor     (LetBind l f)
deriving instance Foldable f    => Foldable    (LetBind l f)
deriving instance Traversable f => Traversable (LetBind l f)

instance Bound (LetBind l) where
  LetBind l p b >>>= f = LetBind l p (b >>>= f)


instance HasLocation (LetBind l f a) where
  locOf (LetBind l _ _) = maybe (error "LetBind missing source annotation") id l


------------------------------------------------------------------------
-- Else Branch Syntax
------------------------------------------------------------------------

data Else (l :: Lang) (f :: * -> *) (a :: *) where
  Else :: Maybe Loc -> f a -> Else l f a
  Elif :: Maybe Loc -> f a -> f a -> Else l f a -> Else l f a


deriving instance Functor f     => Functor     (Else l f)
deriving instance Foldable f    => Foldable    (Else l f)
deriving instance Traversable f => Traversable (Else l f)


instance Bound (Else l) where
  Else l e >>>= f = Else l (e >>= f)
  Elif l p a b >>>= f = Elif l (p >>= f) (a >>= f) (b >>>= f)

instance HasLocation (Else l f a) where
  locOf = \case
    Else l _ -> maybe (error "elif missing source annotation") id l
    Elif l _ _ _ -> maybe (error "elif missing source annotation") id l


------------------------------------------------------------------------
-- Clause Syntax
------------------------------------------------------------------------

data Clause (l :: Lang) (f :: * -> *) (a :: *) where
  Clause :: Maybe Loc -> [Pat l] -> Scope Int f a -> Clause l f a

deriving instance (Functor f)     => Functor     (Clause l f)
deriving instance (Foldable f)    => Foldable    (Clause l f)
deriving instance (Traversable f) => Traversable (Clause l f)

instance Bound (Clause l) where
  Clause l p b >>>= f = Clause l p (b >>>= f)

instance HasLocation (Clause l f a) where
  locOf (Clause l _ _)= maybe (error "Unable to locate clause") id l


-- clause :: Pat -> Exp -> Clause
-- clause p e = Clause (Just (p <++> e)) (bind p e)

-- exClauseBody :: Clause -> Expr l a
-- exClauseBody (Clause _ bnd)
--   = snd <$> unbind bnd

------------------------------------------------------------------------
-- Template Haskell derivations
------------------------------------------------------------------------

-- These are a hassle to maintain
-- and I'm not sure it they are neccessary

-- -- Expr
-- -- deriveEq1   ''Expr
-- instance Eq1 (Expr l) where
--   liftEq eq (EVar a) (EVar b) = eq a b
--   liftEq eq _ _ = undefined
-- -- deriveOrd1  ''Expr
-- instance Ord1 (Expr l) where
--   liftOrd ord (EVar a) (Evar b) = ord a b

-- instance Eq a  => Eq   (Expr l a) where (==) = eq1
-- instance Ord a => Ord  (Expr l a) where compare = compare1


-- -- Else
-- instance  Eq1 (Else l f)
-- instance Ord1 (Else l f)

-- instance (Eq a, Eq1 f)   => Eq   (Else l f a) where (==) = eq1
-- instance (Ord a, Ord1 f) => Ord  (Else l f a) where compare = compare1


-- -- Clause
-- instance  Eq1 (Clause l f)
-- instance Ord1 (Clause l f)

-- instance (Eq a, Eq1 f)   => Eq   (Clause l f a) where (==) = eq1
-- instance (Ord a, Ord1 f) => Ord  (Clause l f a) where compare = compare1

-- -- Alt
-- instance  Eq1 (Alt l f)
-- instance Ord1 (Alt l f)

-- instance (Eq a, Eq1 f)   => Eq   (Alt l f a) where (==) = eq1
-- instance (Ord a, Ord1 f) => Ord  (Alt l f a) where compare = compare1


-- -- Patterns
-- instance  Eq1 (Pat l f)
-- instance Ord1 (Pat l f)

-- instance (Eq a, Eq1 f)   => Eq   (Pat l f a) where (==) = eq1
-- instance (Ord a, Ord1 f) => Ord  (Pat l f a) where compare = compare1


------------------------------------------------------------------------
-- Expression Helpers
------------------------------------------------------------------------

pvars :: Pat l -> [String]
pvars = \case
  PVar v -> [v]
  PCon _ ps -> concatMap pvars ps

  PUnit -> []
  PTuple ps -> concatMap pvars (NE.toList ps)
  PList  ps -> concatMap pvars ps

  PWild -> []
  PAs v p -> v : pvars p

  PType p _ -> pvars p
  PLoc  p _ -> pvars p
  PParens p -> pvars p

------------------------------------------------------------------------
-- Smart Constructors
------------------------------------------------------------------------

-- evar :: String -> Expr l a
-- evar = EVar . s2n

-- eapp :: String -> [Exp l a] -> Expr l a
-- eapp f_n xs = EApp (evar f_n) (NE.fromList xs) 

-- elet :: [(Pat l a, Exp l a)] -> Exp l a -> Expr l a
-- elet qs body = ELet (bind (rec $ NE.fromList (second embed <$> qs)) body)

-- ecase :: Exp l a -> [(Pat l a, Expr l a)] -> Expr l a
-- ecase e qs = ECase e (uncurry clause <$> NE.fromList qs)

-- elam :: [Pat] -> Expr l a -> Expr l a
-- elam ps body = ELam (bind (NE.fromList ps) body)

eletN :: Maybe Loc -> NonEmpty (Pat l, Expr l String) -> Expr l String -> Expr l String
eletN l bs body = maybe let_ (ELoc let_) l
  where let_ = ELetN (letbind_ <$> bs) (abstr body)
        letbind_ (p, e) = LetBind (Just (p<++>e)) p (abstr e)
        vs = concatMap pvars . fmap fst . NE.toList $ bs
        abstr = abstract (`elemIndex` vs)

elamN :: IsIn l '[ 'L1 ]
      => Maybe Loc -> NonEmpty (Pat l) -> Expr l String -> Expr l String
elamN l ps b = maybe lam (ELoc lam) l
  where lam = ELamN ps (abstract (`elemIndex` vs) b)
        vs = concatMap pvars (NE.toList ps)

clause_ :: Maybe Loc -> [Pat l] -> Expr l String -> Clause l (Expr l) String
clause_ l ps b = Clause l ps (abstract (`elemIndex` vs) b)
  where vs = concatMap pvars ps

alt_ :: Maybe Loc -> Pat l -> Expr l String -> Alt l (Expr l) String
alt_ l p e = Alt l p (abstract (`elemIndex` vs) e)
  where vs = pvars p

------------------------------------------------------------------------
-- Simplify Lambda Pass
------------------------------------------------------------------------

-- simplifyLam :: Eq a => Expr 'L1 a -> Expr 'L2 a
-- simplifyLam = \case
--   EVar v
--     -> EVar v

--   ESimpleLam _
--     -> error "Unexpected syntax encountered"

--   ELam xs (simplifyLam -> body)
--     -> foldr (\x e -> ESimpleLam (abstract1 x e)) body xs

--   EApp (simplifyLam -> f) (simplifyLam -> a) 
--     -> EApp f a