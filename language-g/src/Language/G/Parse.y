{

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DataKinds #-}
module Language.G.Parse where

import Language.G.Lex
import Language.G.Lex.Token
import Language.G.Syntax
import Language.G.Syntax.Location

import Data.List.NonEmpty (NonEmpty, (<|))
import qualified Data.List.NonEmpty as NE

import Data.Semigroup

import Data.DList (DList)
import qualified Data.DList as DL

import Data.Text (Text, unpack)
import Data.Text.Prettyprint.Doc

}

%name parseModule module_doc
%tokentype { Token }
-- %lexer { lexNonSpace >>= } { Token TokenEof _ _ }
-- %monad { P } { >>= } { return }
%errorhandlertype explist
%error { expParseError }


%token
  '\\'               { Token (TokenRsvp "\\") _ $$ }
  '->'               { Token (TokenRsvp "->") _ $$ }
  '|'                { Token (TokenRsvp "|") _ $$ }
  ':'                { Token (TokenRsvp ":") _ $$ }
  ':='               { Token (TokenRsvp ":=") _ $$ }
  ','                { Token (TokenRsvp ",") _ $$ }
  '.'                { Token (TokenRsvp ".") _ $$ }
  '='                { Token (TokenRsvp "=") _ $$ }
  '_'                { Token (TokenRsvp "_") _ $$ }
  '+'                { Token (TokenRsvp "+") _ $$ }
  '-'                { Token (TokenRsvp "-") _ $$ }
  '*'                { Token (TokenRsvp "*") _ $$ }
  '&'                { Token (TokenRsvp "&") _ $$ }

  '<['               { Token (TokenRsvp "]>") _ $$ }
  ']>'               { Token (TokenRsvp "<[") _ $$ }

  '{'                { Token (TokenRsvp "{") _ $$ }
  '}'                { Token (TokenRsvp "}") _ $$ }
  '('                { Token (TokenRsvp "(") _ $$ }
  ')'                { Token (TokenRsvp ")") _ $$ }
  '['                { Token (TokenRsvp "[") _ $$ }
  ']'                { Token (TokenRsvp "]") _ $$ }

  'I1'               { Token (TokenRsvp  "I1" ) _ $$ }
  'I8'               { Token (TokenRsvp  "I8" ) _ $$ }
  'I16'              { Token (TokenRsvp "I16" ) _ $$ }
  'I32'              { Token (TokenRsvp "I32" ) _ $$ }
  'I64'              { Token (TokenRsvp "I64" ) _ $$ }

  'U8'               { Token (TokenRsvp  "U8" ) _ $$ }
  'U16'              { Token (TokenRsvp "U16" ) _ $$ }
  'U32'              { Token (TokenRsvp "U32" ) _ $$ }
  'U64'              { Token (TokenRsvp "U64" ) _ $$ }

  'F16'              { Token (TokenRsvp  "F16" ) _ $$ }
  'F32'              { Token (TokenRsvp  "F32" ) _ $$ }
  'F64'              { Token (TokenRsvp  "F64" ) _ $$ }
  'F128'             { Token (TokenRsvp "F128" ) _ $$ }

  'let'              { Token (TokenRsvp "let"   ) _ $$ }
  'in'               { Token (TokenRsvp "in"   ) _ $$ }
  'case'             { Token (TokenRsvp "case"   ) _ $$ }
  'of'               { Token (TokenRsvp "of"   ) _ $$ }

  'if'               { Token (TokenRsvp "if"   ) _ $$ }
  'then'             { Token (TokenRsvp "then"   ) _ $$ }
  'else'             { Token (TokenRsvp "else"   ) _ $$ }
  'elif'             { Token (TokenRsvp "elif"   ) _ $$ }

  'module'           { Token (TokenRsvp "module") _ $$ }
  'import'           { Token (TokenRsvp "import") _ $$ }

  'type'             { Token (TokenRsvp "type" ) _ $$ }
  'as'               { Token (TokenRsvp "as" ) _ $$ }
  'extern'           { Token (TokenRsvp "extern" ) _ $$ }

  varId              { Token (TokenVarId  _) _ _ }
  conId              { Token (TokenConId  _) _ _ }

  '#add'             { Token (TokenPrimId "#add") _ $$ }
  '#sub'             { Token (TokenPrimId "#sub") _ $$ }
  '#mul'             { Token (TokenPrimId "#mul") _ $$ }
  '#div'             { Token (TokenPrimId "#div") _ $$ }
  '#rem'             { Token (TokenPrimId "#rem") _ $$ }
  '#neg'             { Token (TokenPrimId "#neg") _ $$ }

  '#and'             { Token (TokenPrimId "#and") _ $$ }
  '#or'              { Token (TokenPrimId  "#or") _ $$ }
  '#xor'             { Token (TokenPrimId "#xor") _ $$ }
  '#shr'             { Token (TokenPrimId "#shr") _ $$ }
  '#shl'             { Token (TokenPrimId "#shl") _ $$ }

  '#eq'              { Token (TokenPrimId "#eq") _ $$ }
  '#neq'             { Token (TokenPrimId "#neq") _ $$ }
  '#lt'              { Token (TokenPrimId "#lt") _ $$ }
  '#le'              { Token (TokenPrimId "#le") _ $$ }
  '#gt'              { Token (TokenPrimId "#gt") _ $$ }
  '#ge'              { Token (TokenPrimId "#le") _ $$ }


  integer            { Token (TokenInteger _) _ _ }
  double             { Token (TokenDouble  _) _ _ }
  char               { Token (TokenChar    _) _ _ }
  string             { Token (TokenString  _) _ _ }
  boolean            { Token (TokenBool    _) _ _ }
  'null'             { Token (TokenRsvp "null") _ $$ }

  begin_line         { Token TokenLn _ _ }
  end_line           { Token TokenLn' _ _ }

  begin_block        { Token TokenBlk _ _ }
  end_block          { Token TokenBlk' _ _ }

  EOF                { Token TokenEof _ _ }

%%

-- Associativity

-- -----------------------------------------------------------------------------
-- | Helpers

some(p) -- :: { [_] }
  : some_dl(p)        { DL.toList $1 }

some_ne(p) -- :: { NonEmpty _ }
  : some(p)         { NE.fromList $1 }

some_dl(p) -- :: { DList _ }
  : some_dl(p) p   { DL.snoc $1 $2 }
  | p              { DL.singleton $1 }

-- | Zero or more occurences of 'p'
many(p) -- :: { [_] }
  : some(p)     { $1 }
  | {- empty -} { [] }


sep_by1(p,sep) -- :: { [_] }
  : sep_by1_dl(p,sep) { DL.toList $1 }

sep_by1_ne(p,sep) -- :: { NonEmpty _ }
  : sep_by1(p,sep) { NE.fromList $1 }

sep_by1_dl(p,sep) -- :: { DList _ }
  : sep_by1_dl(p,sep) sep p  { DL.snoc $1 $3 }
  | p                        { DL.singleton $1 }

linefold(p) -- :: { _ }
  : begin_line p end_line { $2 }

block(p) -- :: { [_] }
  : begin_block many(linefold(p)) end_block { $2 }

block1(p) -- :: { NonEmpty _ }
  : begin_block some_ne(linefold(p)) end_block { $2 }


-- -----------------------------------------------------------------------------
-- | Names and Values

mod_id :: { L String }
  : con_id { $1 }

var_id :: { L String }
  : varId { fmap unpack (extractId $1) }

con_id :: { L String }
  : conId { fmap unpack (extractId $1) }

literal :: { L Lit }
  : integer  { fmap (LInt . fromInteger) (extractInteger $1) }
  | double   { fmap LFp     (extractDouble $1) }
  | char     { fmap LChar   (extractChar   $1) }
  | boolean  { fmap LBool   (extractBool   $1) }
  | string   { fmap LString (extractString $1) }


-- -----------------------------------------------------------------------------
-- | Definitions

module_doc :: { Module L1 String }
  : linefold(module_defn) many(linefold(defn)) mayeof
    { Module (locOf $1) (unL $1) $2 }

mayeof :: { Maybe Token }
  : {- Empty -} { Nothing }
  | EOF         { Just $1 }

module_defn :: { L String }
  : 'module' con_id { L "" $1 <> $2 }  

defn :: { Stmt 'L1 String }
  : func_defn         { FuncDefn $1 }
  | func_sig          { FuncSig $1 }
  | datatype_defn     { DataTypeDefn $1 }


-- -----------------------------------------------------------------------------
-- | Function

func_sig :: { Sig 'L1 }
  : var_id ':' type { Sig ($1 <++> $3) (unL $1) $3 }

func_defn :: { Func 'L1 (Expr 'L1) String }
  : var_id clause { Func ($1 <++> $2) (unL $1) $2 }

clause :: { Clause 'L1 (Expr 'L1) String }
  : some(apat) '=' exp { clause_ (Just (head $1 <++> $3)) $1 $3 }
  | '=' exp            { clause_ (Just ($1 <++> $2)) [] $2 }


-- -----------------------------------------------------------------------------
-- | Patterns

pat :: { Pat 'L1 }
  : cpat { $1 }

cpat :: { Pat 'L1 }
  : bpat ':' type { PLoc (PType $1 $3) ($1 <++> $3) }
  | bpat { $1 }

bpat :: { Pat 'L1 }
  : con_id some(apat) { PLoc (PCon (unL $1) $2) ($1 <++> $2) }
  | apat { $1 }

apat :: { Pat 'L1 }
  : var_id      { PLoc (PVar (unL $1)) (locOf $1)  }
  | con_id      { PLoc (PCon (unL $1) []) (locOf $1) }
  | '_'         { PLoc PWild (locOf $1) }
  | '(' pat ')' { PLoc (PParens $2) ($1<>$3) }
  | '(' pat ',' sep_by1_ne(pat, ',') ')'
                { PLoc (PTuple ($2 <| $4)) ($1<>$5) }


-- -----------------------------------------------------------------------------
-- | DataType

datatype_defn :: { DataType 'L1 }
  : 'type' con_id             { DataType ($1 <++> $2) (unL $2) [] }
  | 'type' con_id '=' constrs { DataType ($1 <++> $4) (unL $2) $4 }

constrs :: { [ConstrDefn 'L1] }
  : sep_by1(constr_defn, '|') { $1 }

constr_defn :: { ConstrDefn 'L1 }
  : con_id
    { ConstrDefn (locOf $1) (unL $1) [] } 
  
  | con_id some(atype)
    { ConstrDefn ($1 <++> $2) (unL $1) $2 }
  
  | con_id '{' sep_by1_ne(entry_defn, ',') '}'
    { RecordDefn ($1 <++> $4) (unL $1) $3 }

entry_defn :: { Entry 'L1 }
  : var_id ':' type { Entry ($1 <++> $3) (unL $1) $3 }


-- -----------------------------------------------------------------------------
-- | Expressions

exp :: { Expr 'L1 String }
  : dexp { $1 }

dexp :: { Expr 'L1 String }
  : cexp ':' type  { ELoc (EType $1 $3) ($1 <++> $3) }
  | cexp { $1 }

cexp :: { Expr 'L1 String }
  : 'let' block1(equation) 'in' cexp  
                                { eletN (Just ($1 <++> $4)) $2 $4 }
  | 'if' bexp 'then' exp elsebr { ELoc (EIf $2 $4 $5) ($1 <++> $5) }
  | 'case' bexp 'of' alts       { ELoc (ECase $2 $4) ($1 <++> $4) }
  | '\\' some_ne(apat) '->' exp { elamN (Just ($1 <++> $4)) $2 $4 }
  | bexp { $1 }

equation :: { (Pat 'L1, Expr 'L1 String) }
  : pat '=' exp { ($1, $3) }

elsebr :: { Else 'L1 (Expr 'L1) String }
  : 'else' exp { Else (Just $ $1 <++> $2) $2 }
  | 'elif' bexp 'then' exp elsebr { Elif (Just $ $1 <++> $5) $2 $4 $5 }

alts :: { NonEmpty (Alt 'L1 (Expr 'L1) String) }
  : block1(alt) { $1 }

alt :: { Alt 'L1 (Expr 'L1) String }
  : pat '->' exp { alt_ (Just ($1 <++> $3)) $1 $3 }

bexp :: { Expr 'L1 String }
  : aexp some_ne(aexp)  { ELoc (EApp $1 $2) ($1 <++> $2) }
  | aexp                { $1 }


aexp :: { Expr 'L1 String }
  : var_id            { ELoc (EVar (unL $1)) (locOf $1) }
  | con_id            { ELoc (ECon (unL $1)) (locOf $1) }
  | literal           { ELoc (ELit (unL $1)) (locOf $1) }
  | primOp            { ELoc (EPrimOp (unL $1)) (locOf $1) }
  
  | '[' sep_by1(exp, ',') ']' 
                      { ELoc (EList $2) ($1<>$3) }
  
  | '(' exp ',' sep_by1_ne(exp, ',') ')'
                      { ELoc (ETuple ($2 <| $4)) ($1<>$5) }
  | '(' exp ')'       { ELoc (EParens $2) ($1<>$3) }
  | '(' ')'           { ELoc EUnit ($1<>$2) }


primOp :: { L PrimOp }
  : '#add' { L OpAdd $1 }
  | '#sub' { L OpSub $1 }
  | '#mul' { L OpMul $1 }
  | '#div' { L OpDiv $1 }
  | '#rem' { L OpRem $1 }
  | '#neg' { L OpNeg $1 }

  | '#eq'  { L OpEq  $1 }
  | '#neq' { L OpNeq $1 }

  | '#lt'  { L OpLT $1 }
  | '#le'  { L OpLE $1 }
  | '#gt'  { L OpGT $1 }
  | '#ge'  { L OpGE $1 }


-- -----------------------------------------------------------------------------
-- | Types

type :: { Type 'L1 } 
  : ctype { $1 }

ctype :: { Type 'L1 }
  : btype '->' ctype { TLoc (TArr $1 $3) ($1<++>$3) }
  | btype { $1 }

btype :: { Type 'L1 }
  : con_id some_ne(atype)
    { TLoc (TCon (unL $1) (NE.toList $2)) (locOf $1 <> sconcat (fmap locOf $2)) }

  | con_id            
    { TLoc (TCon (unL $1) []) (locOf $1) }

  | atype { $1 }

atype :: { Type 'L1 }
  : primType     { TLoc (TPrim $ unL $1) (locOf $1) }
  | '[' type ']' { TLoc (TList $2) ($1<>$3) }
  | '(' type ')' { TLoc (TParens $2) ($1<>$3) }
  | '(' type ',' sep_by1_ne(type, ',') ')'
    { TLoc (TTuple ($2 <| $4)) ($1<>$5) }


primType :: { L PrimType }
  : 'I1'  { L (TInt  1) (locOf $1) }
  | 'I8'  { L (TInt  8) (locOf $1) }
  | 'I16' { L (TInt 16) (locOf $1) }
  | 'I32' { L (TInt 32) (locOf $1) }
  | 'I64' { L (TInt 64) (locOf $1) }

  | 'U8'  { L (TUInt  8) (locOf $1) }
  | 'U16' { L (TUInt 16) (locOf $1) }
  | 'U32' { L (TUInt 32) (locOf $1) }
  | 'U64' { L (TUInt 64) (locOf $1) }

  | 'F16'  { L (TFp  16) (locOf $1) }
  | 'F32'  { L (TFp  32) (locOf $1) }
  | 'F64'  { L (TFp  64) (locOf $1) }
  | 'F128' { L (TFp 128) (locOf $1) }


{

parseError :: [Token] -> a
parseError [] = error "Parse error, no tokens left!!"
parseError (t:_) = error $ "Parse error on token: " ++ show (pretty t)

expParseError :: ([Token], [String]) -> a
expParseError ([], s)  = error $ "Parse error:"
                               ++ "Expected: " ++ show s
expParseError (ts, s) = error $ "Parse error on tokens: " ++ show (pretty $ head ts) ++ "\n"
                               ++ "Expected: " ++ show (punctuate "," $ pretty <$> s)

}
