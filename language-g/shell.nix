with (import ../nix/nixpkgs.nix {});
haskellPackages.shellFor {
  packages = p: with p; [ (import ./. {}) ];
  buildInputs = with pkgs.haskellPackages; [ cabal-install ];
  withHoogle = false;
}