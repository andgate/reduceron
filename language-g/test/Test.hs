{-# LANGUAGE DeriveDataTypeable #-}
module Main where

import Control.Monad
import Data.List

import Test.Tasty (defaultMain, TestTree, testGroup)
import Test.Tasty.Golden (goldenVsFile, findByExtension)

import System.Directory (listDirectory, removeDirectoryRecursive, createDirectoryIfMissing, doesDirectoryExist)
import System.FilePath (takeBaseName, replaceExtension, (</>))
import System.Process (callCommand, waitForProcess)

import System.Exit (exitWith, ExitCode(..), die)

testDir = "golden"
outDir = testDir </> "out"
binDir = testDir </> "bin"
buildDir = testDir </> "build"
testSingleDir = testDir </> "single"
outSingleDir = outDir </> "single"
binSingleDir = binDir </> "single"
buildSingleDir = buildDir </> "single"

testMultiDir = testDir </> "multi"
outMultiDir = outDir </> "multi"
binMultiDir = binDir </> "multi"
buildMultiDir = buildDir </> "multi"

main :: IO ()
main = defaultMain =<< goldenTests


ifM :: Monad m => m Bool -> m () -> m ()
ifM mp mt = do
  p <- mp
  if p then mt else return ()

removeOldTests :: IO ()
removeOldTests = do
  ifM (doesDirectoryExist $ testDir </> "out")
      (removeDirectoryRecursive "tests/out")
  ifM (doesDirectoryExist "tests/build")
      (removeDirectoryRecursive "tests/build")
  ifM (doesDirectoryExist "tests/bin")
      (removeDirectoryRecursive "tests/bin")


goldenTests :: IO TestTree
goldenTests = do
  singleFileTests <- goldenTestSingle
  multiFileTests <- mempty -- goldenTestMulti
  return $ testGroup "G Golden Tests" (singleFileTests <> multiFileTests)


goldenTestSingle :: IO [TestTree]
goldenTestSingle = do
  srcPaths <- findByExtension [".g"] testSingleDir
  createDirectoryIfMissing True outSingleDir
  createDirectoryIfMissing True binSingleDir

  return $
    [ goldenVsFile
        ("Golden Test " ++ takeBaseName srcPath ++ " (Single-File)")
        goldPath
        outPath
        (compileFile srcPath outPath)
    | srcPath <- srcPaths
    , let outPath = outSingleDir </> takeBaseName srcPath <> ".out"
          goldPath = replaceExtension srcPath "gold"
    ]


goldenTestMulti :: IO [TestTree]
goldenTestMulti = do
  srcDirs <- filterM doesDirectoryExist . map (testMultiDir </>) =<< listDirectory testMultiDir
  createDirectoryIfMissing True outDir
  createDirectoryIfMissing True binDir
  return $
    [ goldenVsFile
        ("Golden Test " ++ takeBaseName srcDir ++ " (Multi-File)")
        goldPath
        outPath
        (compileDir srcDir outPath)
    | srcDir <- srcDirs
    , let outPath = outDir </> takeBaseName srcDir <> ".out"
          goldPath = replaceExtension srcDir ".gold"
    ]


compileFile :: FilePath -> FilePath -> IO ()
compileFile srcPath outPath = do
  let exePath = binSingleDir <> takeBaseName srcPath
  let buildPath = buildSingleDir <> takeBaseName srcPath <> "/"
  callCommand $ "g +RTS -xc -RTS " 
             ++ srcPath ++ " -o " ++ exePath
             ++ " --build-dir " ++ buildPath
             ++ " > " ++ outPath
  -- callCommand $ exePath ++ " > " ++ outPath

compileDir :: FilePath -> FilePath -> IO ()
compileDir srcDir outPath = do
  let exePath = binMultiDir <> takeBaseName srcDir
  srcPaths <- findByExtension [".g"] srcDir
  callCommand $ "g " ++ intercalate " " srcPaths ++ " -o " ++ exePath
  callCommand $ exePath ++ " > " ++ outPath