{ nixpkgs ? import ../nix/nixpkgs.nix {} }:
with nixpkgs.pkgs;
let
    inherit (haskellPackages) callCabal2nix;
    language-t = haskell.lib.dontHaddock 
      (callCabal2nix "language-t" ../language-t {});
    language-g = callCabal2nix "language-g" ./. { language-t = language-t; };
in
# Tests are disabled in nix (golden testing modifies the source)
haskell.lib.dontCheck
  (callCabal2nix "language-g" ./. { language-t = language-t; })