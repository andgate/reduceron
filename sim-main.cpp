#include "VSky_testBench.h"
#include "verilated.h"

int main(int argc, char** argv, char** env)
{
    Verilated::commandArgs(argc, argv);
    VSky_testBench* top = new VSky_testBench();
    while (!Verilated::gotFinish()) { top->eval(); }
    delete top;
    exit(0);
}