{-# LANGUAGE TemplateHaskell
           , ScopedTypeVariables
           , DataKinds
           , DeriveGeneric
           #-}
module Reduceron.Types where

import Clash.Prelude
import Reduceron.Bytecode

import GHC.Generics

data Registers
  = Registers
  { _regX :: Atom , _regY :: Atom , _regZ :: Atom
  -- ^ Temporaries
  , _regTop :: Atom
  -- ^ Top of stack
  , _regHP :: HeapAddr
  -- ^ Heap Pointer
  , _regSP :: StackAddr 
  -- ^ Stack Pointer
  , _regRoot :: HeapAddr
  -- ^ Redex root in heap
  , _regBase :: HeapAddr
  -- ^ Base address of function instantiation

  , _fpuStage :: Stage
  , _fpuIsHalted :: Bool
  , _fpuOutReg :: FPUOut
  } deriving (Generic)

instance NFDataX Registers 

data FPUIn = FPUIn
  { fpuInHeapRead :: App
  , fpuInStackRead :: Atom
  , fpuInTemplatesRead :: Template
  } deriving (Generic)

instance NFDataX FPUIn 

data FPUOut = FPUOut 
  { fpuOutHeapRead :: HeapAddr
  , fpuOutHeapWrite :: Maybe (HeapAddr, App)
  , fpuOutStackRead :: StackAddr
  , fpuOutStackWrite :: Maybe (StackAddr, Atom)
  , fpuOutTemplatesRead :: Template
  } deriving (Generic)

instance NFDataX FPUOut

emptyFPUOut = FPUOut
  { fpuOutHeapRead = undefined
  , fpuOutHeapWrite = undefined
  , fpuOutStackRead = undefined
  , fpuOutStackWrite = undefined
  , fpuOutTemplatesRead = undefined
  }

initialRegisters :: Registers
initialRegisters
  = Registers
      { _regX = undefined
      , _regY = undefined
      , _regZ = undefined 
      , _regTop = undefined
      , _regHP = undefined
      , _regSP = undefined
      , _regRoot = undefined
      , _regBase = undefined
      , _fpuStage = undefined
      , _fpuIsHalted = undefined
      , _fpuOutReg = emptyFPUOut
      }