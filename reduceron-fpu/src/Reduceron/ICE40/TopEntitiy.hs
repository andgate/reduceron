module Reduceron.Ice40.HX8K.CT256.TopEntity where



{-# ANN topEntity
  (Synthesize
    { t_name   = "reduceron"
    , t_inputs = [ PortName "CLOCK_50"
                 , PortName "KEY0"
                 , PortName "KEY1" ]
    , t_output = PortName "LED"
    }) #-}

topEntity :: 