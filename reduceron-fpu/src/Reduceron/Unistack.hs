module Reduceron.Unistack where


import Clash.Explicit.Prelude
import Clash.Extra.BlockRamDP (blockRamDP')


-- A value of type 'Unistack n m' is a stack supporting pushing and
-- popping of a single element of type 'Word m' per clock-cycle.
-- Pushing and popping can be done in parallel.  The maximum number of
-- elements that can be stored on the stack is '2^n'.


