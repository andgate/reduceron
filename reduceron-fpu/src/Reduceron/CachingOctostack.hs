{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Reduceron.CachingOctostack where

import Clash.Extra.BlockRam (blockRam')
import Clash.Explicit.Prelude
import Clash.Explicit.BlockRam hiding (blockRam#)


-- A caching octostack is like an octostack except that the top 8
-- elements are cached in registers to reduce propagation delay; there
-- is no delay when reading from the stack, but there is still a small
-- delay when writing.  For good performance, compute the 'offset'
-- parameter to the 'update' operation quickly.  See Memo 27 for
-- details.

-- In general, the offset is equal to the nubmer of pushes minus
-- then umber of pops
--   offset = pushN - popN

{-# NOINLINE decodeOneHot #-}
decodeOneHot :: BitVector 3 -> BitVector 8
decodeOneHot i = case i of
  $(bitPattern "000") -> $$(bLit "00000001") :: BitVector 8
  $(bitPattern "001") -> $$(bLit "00000010")
  $(bitPattern "010") -> $$(bLit "00000100")
  $(bitPattern "011") -> $$(bLit "00001000")
  $(bitPattern "100") -> $$(bLit "00010000")
  $(bitPattern "101") -> $$(bLit "00100000")
  $(bitPattern "110") -> $$(bLit "01000000")
  $(bitPattern "111") -> $$(bLit "10000000")

{-# NOINLINE mkWriteMask #-}
mkWriteMask :: Maybe (Unsigned 3) -> BitVector 8
mkWriteMask Nothing = $$(bLit "00000000") :: BitVector 8
mkWriteMask (Just i) = case (bitCoerce i :: BitVector 3) of
  $(bitPattern "000") -> $$(bLit "00000001") :: BitVector 8
  $(bitPattern "001") -> $$(bLit "00000011")
  $(bitPattern "010") -> $$(bLit "00000111")
  $(bitPattern "011") -> $$(bLit "00001111")
  $(bitPattern "100") -> $$(bLit "00011111")
  $(bitPattern "101") -> $$(bLit "00111111")
  $(bitPattern "110") -> $$(bLit "01111111")
  $(bitPattern "111") -> $$(bLit "11111111")


{-# INLINE prod2Vec8 #-}
prod2Vec8 :: forall a. (a, a, a, a, a, a, a, a) -> Vec 8 a
prod2Vec8 (a1, a2, a3, a4, a5, a6, a7, a8)
  = a1:>a2:>a3:>a4:>a5:>a6:>a7:>a8:>Nil

{-# NOINLINE computeOffset #-}
computeOffset
  :: forall addr. KnownNat addr
  => Signed 4
  -> Unsigned addr
  -> Unsigned addr
computeOffset offset addr =
  case bitToBool (msb offset) of
    True  -> addr - offset'
      where offset' :: Unsigned addr
            offset' = resize . bitCoerce . negate $ offset
    False -> addr + offset'
      where offset' :: Unsigned addr
            offset' = resize . bitCoerce $ offset


{-# NOINLINE computeOffsets #-}
computeOffsets
  :: forall addr n.
     ( KnownNat addr
     , KnownNat n )
  => Signed 4
  -> Vec n (Unsigned addr)
  -> Vec n (Unsigned addr)
computeOffsets offset addrs
  = computeOffset offset <$> addrs


{-# NOINLINE popStackPtr #-}
popStackPtr
  :: forall addr.
     ( KnownNat addr )
  => Maybe (Unsigned 3) 
  -> Unsigned addr
  -> Unsigned addr
popStackPtr Nothing sp = sp 
popStackPtr (Just popN) sp
  | sp <= resize popN + 1 = 0
  | otherwise = sp - (resize popN + 1)

{-# NOINLINE pushStackPtr #-}
pushStackPtr
  :: forall addr.
     ( KnownNat addr )
  => Maybe (Unsigned 3) 
  -> Unsigned addr
  -> Unsigned addr
pushStackPtr Nothing sp = sp 
pushStackPtr (Just pushN) sp = sp + resize pushN + 1

{-# NOINLINE computePushN #-}
computePushN
  :: forall addr.
     ( KnownNat addr )
  => Maybe (Unsigned 3)
  -> Unsigned addr
computePushN Nothing = 0
computePushN (Just pushN) = resize pushN + 1


{-# INLINE computeRot #-}
computeRot :: forall addr. KnownNat addr => Unsigned addr -> BitVector 3
computeRot sp = bitCoerce $ resize $ (sp `mod` (8 :: Unsigned addr))

{-# NOINLINE computeStackSize #-}
computeStackSize :: forall addr. KnownNat addr => Unsigned addr -> Maybe (Unsigned 3)
computeStackSize sp
  | sp == 0   = Nothing
  | sp >= 8   = Just 7
  | otherwise = Just (resize sp - 1)

{-# INLINE checkBit #-}
checkBit :: forall a i. (BitPack a, KnownNat (BitSize a), Enum i)
         => a -> i -> Bool
checkBit a i = bitToBool (a!i)

-- izipWith
--   :: forall a b c.
--      (a -> b -> c) -> Vec n a -> Vec n b -> Vec n c
-- izipWith _ Nil           _  = Nil
-- izipWith f (x `Cons` xs) ys = f x (head ys) `Cons` izipWith f xs (tail ys)
-- {-# NOINLINE izipWith #-}

-- ifoldl :: forall a b n. 
--           (forall i. (KnownNat i) => b -> i -> a -> b)
--           -> b -> Vec n a -> b
-- ifoldl f z xs = 

-- barrelRotL 
--   :: forall a n m.
--      ( KnownNat n
--      , KnownNat m
--      , (2^n) ~ m)
--      => Vec m a       -- ^ Initial vector
--      -> BitVector n   -- ^ Rotation magnitude
--      -> Vec m a       -- ^ Intial vector rotated left by `n` elements 
-- barrelRotL din s = fst $ foldl go (din, d0) (bv2v s) 
--   where go (din', i) b
--         go (din', i) b
--           | bitToBool b = (rotateLeftS din' (powSNat 2 i), succSNat i)
--           | otherwise   = din'

{-# NOINLINE barrelRotL #-}
barrelRotL :: forall a. BitVector 3 -> Vec 8 a -> Vec 8 a
barrelRotL s din0 = dout
  where
    din1 | checkBit s 0 = rotateLeftS din0 d1
         | otherwise       = din0
    din2 | checkBit s 1 = rotateLeftS din1 d2
         | otherwise       = din1
    dout | checkBit s 2 = rotateLeftS din2 d4
         | otherwise       = din2

{-# NOINLINE barrelRotR #-}
barrelRotR :: forall a. BitVector 3 -> Vec 8 a -> Vec 8 a
barrelRotR s din0 = dout
  where
    din1 | bitToBool (s!0) = rotateRightS din0 d1
         | otherwise   = din0
    din2 | bitToBool (s!1) = rotateRightS din1 d2
         | otherwise   = din1
    dout | bitToBool (s!2) = rotateRightS din2 d4
         | otherwise   = din2

{-# NOINLINE safeSub #-}
safeSub a b
  | a > b = a - b
  | otherwise = 0

{-# NOINLINE translateAddr #-}
translateAddr :: forall addr. KnownNat addr => Unsigned addr -> Unsigned addr
translateAddr addr = (addr - ramI) `div` 8
  where ramI = addr `mod` 8

{-# NOINLINE computeTopAddrs #-}
computeTopAddrs :: forall addr. KnownNat addr => BitVector 3 -> Unsigned addr -> Vec 8 (Unsigned addr)
computeTopAddrs r top = barrelRotR r $ map (safeSub top) (0:>1:>2:>3:>4:>5:>6:>7:>Nil)

{-# NOINLINE computeRamTops #-}
computeRamTops :: forall addr. KnownNat addr => BitVector 3 -> Unsigned addr -> Vec 8 (Unsigned addr)
computeRamTops r top = translateAddr <$> computeTopAddrs r top

{-# NOINLINE computeWriteMask #-}
computeWriteMask :: BitVector 3 -> Maybe (Unsigned 3) -> Vec 8 Bool
computeWriteMask r pN = bitToBool <$> barrelRotR r (bv2v $ mkWriteMask pN)

{-# NOINLINE octostack #-}
octostack
  :: forall a ramSize bramSize addr addr' dom. 
     ( KnownDomain dom
     , BitPack a
     , KnownNat (BitSize a)
     , KnownNat addr
     , KnownNat addr'
     , addr ~ (addr' + 3)
     , KnownNat ramSize
     , KnownNat bramSize
     , bramSize ~ Div ramSize 8)
  => Clock dom
  -> Enable dom
  -> SNat ramSize                        -- ^ RamSize
  -> SNat addr                           -- ^ Address Size
  -> Signal dom (Maybe (Unsigned 3))     -- ^ Pop up to 8 elements (pops are performed before pushes)
  -> Signal dom (Maybe (Unsigned 3))     -- ^ Push up to 8 elements
  -> Signal dom (Vec 8 a)                -- ^ Data in, for elements to push. So upper element, specified by pushN, will be at the top of the stack.
  -> ( Signal dom (Maybe (Unsigned 3))   -- ^ Stack Size
     , Signal dom (Vec 8 a) )            -- ^ Top 8 Elements of the Stack
octostack clk en ramSize addrSize mayPopN mayPushN dataIn
  = (size, douts)
  where
    ramInit :: Vec bramSize a
    ramInit = replicate (divSNat ramSize d8) (bitCoerce (0 :: Unsigned (BitSize a)))

    pushN :: Signal dom (Unsigned addr)
    pushN = computePushN <$> mayPushN

    -- NB Pop before push
    sp, poppedSp, pushedSp :: Signal dom (Unsigned addr)
    sp       = delay clk en 0 pushedSp
    poppedSp = popStackPtr  <$> mayPopN  <*> sp
    pushedSp = pushStackPtr <$> mayPushN <*> poppedSp

    size, newSize :: Signal dom (Maybe (Unsigned 3))
    size    = computeStackSize <$> sp
    newSize = computeStackSize <$> pushedSp
    
    rot, rotPopped, rotPushed :: Signal dom (BitVector 3)
    rot        = delay clk en (boolToBV False) rotPushed
    rotPopped  = computeRot <$> poppedSp
    rotPushed  = computeRot <$> pushedSp
    

    ramReads    = computeRamTops <$> rot <*> sp
    ramWriteEns = computeWriteMask <$> rotPopped <*> mayPushN
    ramWrites   = computeRamTops <$> rotPushed  <*> pushedSp
    ramIns      = barrelRotR <$> rotPopped <*> dataIn

    mkRamN :: Enum n => n -> Signal dom a
    mkRamN n = blockRam' clk en ramInit ((!!n) <$> ramReads) ((!!n) <$> ramWriteEns) ((!!n) <$> ramWrites) ((!!n) <$> ramIns)

    douts   = barrelRotL <$> rot <*> ramOuts
    ramOuts = bundle $ map mkRamN $(listToVecTH [(0 :: Int) .. 7])


{-# NOINLINE cachingOctostack #-}
cachingOctostack
  :: forall a ramSize bramSize addr addr' dom. 
     ( KnownDomain dom
     , BitPack a
     , KnownNat (BitSize a)
     , KnownNat addr
     , KnownNat addr'
     , addr ~ (addr' + 3)
     , KnownNat ramSize
     , KnownNat bramSize
     , bramSize ~ Div ramSize 8)
  => Clock dom
  -> Enable dom
  -> SNat ramSize               -- ^ Ram Size
  -> SNat addr                  -- ^ Address Size
  -> Signal dom (Maybe (Unsigned 3))     -- ^ Pop up to 8 elements (pops are performed before pushes)
  -> Signal dom (Maybe (Unsigned 3))     -- ^ Push up to 8 elements
  -> Signal dom (Vec 8 a)                -- ^ Data in, for elements to push. So upper element, specified by pushN, will be at the top of the stack.
  -> ( Signal dom (Maybe (Unsigned 3))   -- ^ Stack Size
     , Signal dom (Vec 8 a) )            -- ^ Top 8 Elements of the Stack
cachingOctostack clk en ramSize addrSize mayPopN mayPushN dataIn
  = (size, ramOuts)
  where
    (size, ramOuts) = octostack clk en ramSize addrSize mayPopN mayPushN dataIn

    -- pushMask = writeEn
    -- popMask = genPopMask <$> offset

    -- rotated = barrelRotR right regOuts
    -- regIns = zipWith5 choose <$> dataIn <*> pushMask <*> ramOuts <*> popMask <*> rotated
    -- regOuts = register clk en zeroes regIns
    
    -- zeroes = repeat (bitCoerce 0)

-- genPopMask :: Signed 4 -> Vec 8 Bool
-- genPopMask offset = map (\i -> (-i) >= offset) $(listToVecTH [(-1::Signed 4) .. (-8)])

-- choose :: a -> Bit -> a -> Bit -> a -> a
-- choose push pushBit pop popBit rot
--   | bitToBool pushBit = push
--   | bitToBool popBit  = pop
--   | otherwise         = rot


$(decLiteralD 4000)

-- {-# ANN topEntity
--   (Synthesize
--     { t_name   = "cachingOctostack"
--     , t_inputs = [ PortName "clk"
--                  , PortName "mpopN"
--                  , PortName "mpushN"
--                  , PortName "dataIn"
--                  ]
--     , t_output = PortProduct
--                  [ PortName "size"
--                  , PortName "douts"
--                  ]
--     }) #-}
topEntity
  :: Clock System
  -> Signal System (Maybe (Unsigned 3))   -- ^ Pop up to 8 elements (pops are performed before pushes)
  -> Signal System (Maybe (Unsigned 3))   -- ^ Push up to 8 elements
  -> Signal System (Vec 8 (Unsigned 18))  -- ^ Data in
  -- Outputs
  -> ( Signal System (Maybe (Unsigned 3))    -- ^ Stack size
     , Signal System (Vec 8 (Unsigned 18)))  -- ^ Data Outs
topEntity clk =
  cachingOctostack clk en d400 d13 
  where en = enableGen
{-# NOINLINE topEntity #-}