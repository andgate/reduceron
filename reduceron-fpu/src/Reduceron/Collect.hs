module Reduceron.Collect where


{-

The garbage collector
---------------------

Algorithm:

  1. Dump the stack onto the heap.
  2. Copy the root of the graph onto to-space.
  3. Apply the copying collector.
  4. Update pointers on the update stack.
  5. Copy compacted graph back to the heap.

-}