{-# LANGUAGE DeriveAnyClass  #-}
{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections   #-}
{-# LANGUAGE PatternSynonyms #-}
module Reduceron.FPU where


import Clash.Prelude
import Reduceron.Bytecode



type StackPtr = Int
type StackVal = Int

type AStackPtr = Int
type AStackVal = Int

type HeapPtr = Int
type HeapVal = Int

type CodePtr = Int
type CodeVal = Int

data MemIn
  = MemIn
  { stackIn :: StackVal
  , astackIn :: AStackVal
  , heapIn :: AStackVal
  , codeIn :: AStackVal
  }

data MemOut 
  = MemOut
  { stackRead :: StackPtr
  , astackRead :: AStackPtr
  , heapRead :: HeapPtr
  , codeRead :: HeapPtr
  , stackWrite :: Maybe (StackPtr, StackVal)
  , astackWrite :: Maybe (AStackPtr, AStackVal)
  -- , heapWrite :: 
  }

fpu :: HiddenClockResetEnable dom
  => Signal dom MemIn -> Signal dom MemOut
fpu memIn = return memout
  where
    regRead = systemRegisters regWrite
    (regWrite, memout) = unbundle $ process regRead memIn


data RegRead
  = RegRead
  { _regX      :: Int
  , _regY      :: Int
  , _regZ      :: Int
  , _regTop    :: Int
  , _regSp     :: Int
  , _regHp     :: Int
  , _regRoot   :: Int
  , _regBase   :: Int
  , _regI      :: Int
  , _regEnd    :: Int
  }

data RegWrite
  = RegWrite
  { _regwX        :: Maybe Int
  , _regwY        :: Maybe Int
  , _regwZ        :: Maybe Int
  , _regwTop      :: Maybe Int
  , _regwSp       :: Maybe Int
  , _regwHp       :: Maybe Int
  , _regwRoot     :: Maybe Int
  , _regwBase     :: Maybe Int
  , _regwI        :: Maybe Int
  , _regwEnd      :: Maybe Int
  }


nullRegW :: RegWrite
nullRegW
  = RegWrite
  { _regwX        = Nothing
  , _regwY        = Nothing
  , _regwZ        = Nothing
  , _regwTop      = Nothing
  , _regwSp       = Nothing
  , _regwHp       = Nothing
  , _regwRoot     = Nothing
  , _regwBase     = Nothing
  , _regwI        = Nothing
  , _regwEnd      = Nothing
  }


writeRegX :: Int -> RegWrite -> RegWrite
writeRegX x r = r { _regwX = Just x }

writeRegY :: Int -> RegWrite -> RegWrite
writeRegY y r = r { _regwY = Just y }

writeRegZ :: Int -> RegWrite -> RegWrite
writeRegZ z r = r { _regwZ = Just z }

writeRegTop :: Int -> RegWrite -> RegWrite
writeRegTop top r = r { _regwTop = Just top }

writeRegSp :: Int -> RegWrite -> RegWrite
writeRegSp sp r = r { _regwSp = Just sp }

writeRegHp :: Int -> RegWrite -> RegWrite
writeRegHp hp r = r { _regwHp = Just hp }

writeRegRoot :: Int -> RegWrite -> RegWrite
writeRegRoot root r = r { _regwRoot = Just root }

writeRegBase :: Int -> RegWrite -> RegWrite
writeRegBase base r = r { _regwBase = Just base }

writeRegI :: Int -> RegWrite -> RegWrite
writeRegI i r = r { _regwI = Just i }

writeRegEnd :: Int -> RegWrite -> RegWrite
writeRegEnd end r = r { _regwEnd = Just end }


systemRegisters :: HiddenClockResetEnable dom
  => Signal dom RegWrite
  -> Signal dom RegRead
systemRegisters r spIn
  = RegRead <$> x <*> y <*> z
            <*> top <*> sp <*> root
            <*> base <*> i <*> end

  where x    = regMaybe 0 (_regwX    <$> r)
        y    = regMaybe 0 (_regwY    <$> r)
        z    = regMaybe 0 (_regwZ    <$> r)
        top  = regMaybe 0 (_regwTop  <$> r)
        sp   = regMaybe 0 (_regwSp   <$> r)
        hp   = regMaybe 0 (_regwHp   <$> r)
        root = regMaybe 0 (_regwRoot <$> r)
        base = regMaybe 0 (_regwBase <$> r)
        i    = regMaybe 0 (_regwI    <$> r)
        end  = regMaybe 0 (_regwEnd  <$. r)


type Step = Int

data Phase
  = SwapPhase
  | PrimPhase
  | UnwindPhase
  | UnfoldPhase
  | InstPhase

data ProcIn
  = ProcIn
  { _procReg :: RegRead
  , _procMemIn :: MemIn
  }

data ProcOut
  = ProcOut
  { _procRegWrites :: RegWrite
  , _procMemOut :: MemOut
  }

data ProcState
  = ProcState
  { _procPhase :: Phase
  , _procStep  :: Int
  }

nextState :: Atom -> Bit -> 
nextState top doUpdate stackSize heapSize collecting =
  unwind +> update +> swap +> low +> unfold +> gc +> halt +> vempty
  where
    unwind = inv collecting <&> isAP top
    update = inv collecting <&> inv unwind <&> doUpdate
    swap   = inv collecting <&> inv doUpdate <&> isINT top <&> orG stackSize
    unfold = inv collecting <&> inv gc <&> inv doUpdate <&>
              (isCON top <|> isFUN top)
    gc     = collecting <|> (inv doUpdate <&> isHeapFull heapSize <&>
              ((isFUN top <&> funFirst top) <|> isCON top))
    halt   = inv collecting <&> inv doUpdate <&> isINT top <&>
               inv (orG stackSize)


initProcState :: ProcState
initProcState
  = ProcState
  { _procPhase = SwapPhase
  , _procStep = 0
  }


process :: HiddenClockResetEnable dom
  => Signal dom (RegRead, MemIn)
  -> Signal dom (ProcOut)
process sigIn
  = mealy phaseStep initProcState procIn
  where procIn = fmap (\(r, m) -> ProcIn r m) sigIn


phaseStep :: ProcIn -> ProcState -> (ProcState, (RegWrite, MemOut))
phaseStep p = f p
  where f = case procPhase p of -- Next state is not determined by current state 
              SwapPhase -> swap
              PrimPhase -> prim


{-
  swap = top <= s[sp+1]
       | s[sp+1] <- top
       | skip
-}
swap :: Process (RegWrite, MemOut)
swap = case procStep p of
  1 -> do
    sp1 <- succ <$> view (procReg . regSp)
    procStep += 1
    let regWrite = nullRegW
        memOut = MemOut sp1 0 Nothing Nothing
    return (regs, memOut)

  2 -> do
    sp1 <- succ <$> view (procReg . regSp)
    topSp1 <- view (procMemIn . memInStackVal) 
    top <- view (procReg . regTop)
    procPhase %= succ
    procStep .= 0
    let regWrite = writeRegTop top nullRegW
        memOut = MemOut sp1 0 (Just (sp1, top)) Nothing
    return (regWrite, memOut)


{-
  prim = x <= s[sp+1]
       | y <= s[sp+2]
       | root <= a[sp+2]
       | top <- perform top x y
       | sp <- sp+2; h[root] <- End top
       | skip
-}
prim :: Process (RegWrite, MemOut)
prim p regs = case procStep p of
  0 -> do
    sp1 <- succ <$> view (procReg . regSp)
    procStep += 1
    let regWrite = nullRegW 
        memOut = MemOut sp1 0 Nothing Nothing
    return (regWrite, memOut)

  1 -> do
    sp1 <- (+1) <$> view (procReg . regSp)
    let sp2 = sp1 + 1
    x <- view (procMemIn . memInStackVal)
    procStep += 1
    let regWrite = writeRegX x nullRegW
        memOut = MemOut sp2 0 Nothing Nothing
    return (regWrite, memOut)

  2 -> do
    sp2 <- (+2) <$> view (procReg . regSp)
    y <- view (procMemIn . memInStackVal)
    x <- view (procReg . regX)
    top <- view (procReg . regTop)
    let top' = perform top x y
    procStep += 1
    let regWrite = writeRegTop top' . writeRegSp sp2 $ nullRegW
        memOut = MemOut 0 sp2 Nothing Nothing
    return (regWrite, memOut)

  3 -> undefined
  4 -> undefined
  5 -> undefined


perform :: Int -> Int -> Int -> Int
perform op x y = 0

{-s
  unwind = x <- h[contents top]
         ; i <- contents top
         | do ( a[sp] <- i
              | s[sp] <- unEnd x
              ; top <- unEnd x
              ; end <- isEnd x
              ; i <- i + 1
              | if (not end) (x <= h[i] ; sp <- sp-1 | skip)
              ) until end
-}


{-
  unfold = x <= c[contents top]
         ; i <- 1 + contents top
         ; base <- hp
         | y <= c[i]
         | root <= a[sp + arity x]
         | repeat (size x)
             ( y <= c[i+1] ; i <- i + 1
             | inst id y
             | skip
             )
         ; sp <- sp + arity x
         ; h[root] <- End (Ap base)
         ; top <- Ap base
         | unwind
-}


{-
  inst f = \case
    End n -> inst End n

    Ap  n -> h[hp] <- f (Ap (s[base+n-1])) ; hp <- hp + 1

    Var n -> z <= s[sp+n+1]
           | skip
           | h[hp] <- f z ; hp <- hp + 1

    n     -> h[hp] <- f n ; hp <- hp + 1
-}