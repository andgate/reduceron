module Reduceron.Heap where


-- A value of type 'Heap n m' is a dual-port random-access memory
-- storing '2^n' elements elements of type 'Word m'.  A heap also has
-- a size associated with it, and provides 'snoc' operations for
-- appending elements onto the end.  Two snocs (one on each port) can
-- be performed in one clock-cycle, with the effect that the 'snocA'
-- occurs before the 'snocB'.  Currently, a 'snocB' must only ever be
-- performed in parallel with a 'snocA', never alone.

