module Reduceron.Bytecode where

import Clash.Prelude
import Data.Bits

{-

  Size and type of each parallel memory unit

  +------------------+----------+------------------+----------+
  |   Memory Unit    | Element  | Bits per Element | Elements |
  +------------------+----------+------------------+----------+
  | Program          | Template |              234 | 1k       |
  | Heap             | App      |               77 | 8k       |
  | Reduction stack  | Atom     |               18 | 4k       |
  | Update stack     | Update   |               28 | 4k       |
  | Case-table stack | Atom     |               18 | 4k       |
  | Copy space       | App      |               77 | 16k      |
  +------------------+----------+------------------+----------+

  Total required kbits: 2410 kbits
  Available on iCE40:   128 kbits

-}


{-

Pointer tagging scheme:

  +------------------+-----------------------------------------+
  | Tag (LSB first)  | Contents                                |
  +------------------+-----------------------------------------+
  | 000              | Function arity and address              | isFUN
                       arity:3 addr:10 .. first:1
  | 001              | Primitive function arity and identifier | isADD, isSUB, ...
                       arity:3 swap:1 add:1 sub:1 eq:1 neq:1 leq:1 and:1 ...
  | 010              | Pointer to application                  | isAP
                       pointer:...
  | 011              | Pointer to shared application           | isAP isShared
                       pointer:...
  | 100              | Primitive integer                       | isINT
  | 101              | Constructor arity (+1) and index        | isCON
                       arity:3 index:10
  | 110              | Reference to function argument          | isARG
                       shared:1 index:8
  | 111              | Reference to register                   | isREG
                       shared:1 index:8
  +------------------+-----------------------------------------+

Assumptions:

  * Max function arity = 7
  * Max constructor arity = 6

-}


type HeapAddrN        = ToSpaceAddrN
type HeapAddr         = Unsigned HeapAddrN
type StackAddrN       = 10  -- More complex programs need greater than 512
type StackAddr        = Unsigned StackAddrN
type ArityN           = 3
type Arity            = Unsigned 3
type FunAddrN          = 10

type FunAddr = Unsigned FunAddrN

type ToSpaceAddrN = 13  -- Half of heap. HERE IT IS, THE MAIN PARAMETER.
type ToSpaceAddr = Unsigned 13

type UStackAddrN = 9  -- 6 should be enough
type LStackAddrN = 9  -- 9 should be enough
type UpdateN = 24  -- (UStackAddrN + HeadAddrN)
type Update  = Unsigned UpdateN

type AtomN = 3
type Atom  = Unsigned AtomN

type AppN = 77 -- 5 + 4 * atomN ew
type App = Unsigned AppN

{-

Application structure:

  +------------+---------------+
  | Field      | Size (bits)   |
  +------------+---------------+
  | Arity      | 2             |
  | NF         | 1             |
  | HasAlts    | 1             |
  | Collected  | 1             |
  | Atoms      | 4x18          |
  +------------+---------------+

Total width of application = 77 bits

-}

{-

Template structure:

  +------------+---------------+
  | Field      | Size (bits)   |
  +------------+---------------+
  | Offset     | 4             |
  | Top        | 18            |
  | PushAlts   | 1             |
  | Alts       | 10            |
  | InstAtoms2 | 1             |
  | App2Header | 5             |
  | PushMask   | 5             |
  | App2Atoms  | 5x18          |
  | InstApp1   | 1             |
  | App1       | 5 + 4x18      |
  | App1Prim   | 1             |
  | App2Prim   | 1             |
  | DestReg1   | 4             |
  | DestReg2   | 4             |
  +------------+---------------+

Total width of template = 222 bits

NB: The actual values are different - these are just illustrations

-}

type TemplateN = 222 -- 10 * atomN + 42  ew
type Template  = Unsigned TemplateN