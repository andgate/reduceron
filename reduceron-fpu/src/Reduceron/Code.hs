module Reduceron.Code where


import Clash.Explicit.Prelude
import Clash.Extra.BlockRam

-- 'Code' is a read-only random-access memory
-- storing elements of type 'Unsigned a'.

code
  :: forall addr a n.
     ( KnownDomain dom
     , KnownNat addr
     , KnownNat a ) 
     => Clock dom
     -> Enable dom
     -> Vec n Integer           -- ^ Code contents
     -> Signal (Unsigned addr)  -- ^ Read address  
     -> Signal (Unsigned a)     -- ^ Data Output
code clk en init addr
  = blockRam' clk en init addr (pure False) (pure 0) (pure 0)