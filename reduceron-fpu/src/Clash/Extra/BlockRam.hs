{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
module Clash.Extra.BlockRam where

import           Clash.Annotations.Primitive
import           Clash.Prelude
import           Data.Maybe             (isJust)
import           GHC.TypeNats
import           GHC.Stack              (HasCallStack, withFrozenCallStack)
import           Data.String.Interpolate      (i)
import           Data.String.Interpolate.Util (unindent)
import           Clash.XException
  (maybeIsX, seqX, NFDataX, deepErrorX, defaultSeqX, errorX)


{-# ANN blockRam# (InlinePrimitive Verilog $ unindent [i|
  [ { "BlackBox" :
      { "name" : "Clash.Extra.BlockRam.blockRam#"
      , "kind": "Declaration"
      , "type" :
"blockRam#
  :: forall n dom addr a.
     ( KnownDomain dom            --       ARG[0]
     , HasCallStack               --       ARG[1]
     , KnownNat a )               --       ARG[2]
  => Clock dom                    -- clk,  ARG[3]
  -> Enable dom                   -- en,   ARG[4]
  -> Vec n a                      -- init, ARG[5]
  -> Signal dom (BitVector addr)  -- rd,   ARG[6]
  -> Signal dom Bool              -- wren, ARG[7]
  -> Signal dom (BitVector addr)  -- wr,   ARG[8]
  -> Signal dom (BitVector a)     -- din,  ARG[9]
  -> Signal dom (BitVector a)"
      , "outputReg" : true
      , "template" :
"// blockRam begin
reg ~TYPO ~GENSYM[~RESULT_RAM][1] [0:~LENGTH[~TYP[5]]-1];
reg ~TYP[5] ~GENSYM[ram_init][3];
integer ~GENSYM[i][4];
initial begin
  ~SYM[3] = ~CONST[5];
  for (~SYM[4]=0; ~SYM[4] < ~LENGTH[~TYP[5]]; ~SYM[4] = ~SYM[4] + 1) begin
    ~SYM[1][~LENGTH[~TYP[5]]-1-~SYM[4]] = ~SYM[3][~SYM[4]*~SIZE[~TYPO]+:~SIZE[~TYPO]];
  end
end
~IF ~ISACTIVEENABLE[4] ~THEN
always @(~IF~ACTIVEEDGE[Rising][0]~THENposedge~ELSEnegedge~FI ~ARG[3]) begin : ~GENSYM[~RESULT_blockRam][5]~IF ~VIVADO ~THEN
  if (~ARG[4]) begin
    if (~ARG[7]) begin
      ~SYM[1][~ARG[8]] <= ~ARG[9];
    end
    ~RESULT <= ~SYM[1][~ARG[6]];
  end~ELSE
  if (~ARG[7] & ~ARG[4]) begin
    ~SYM[1][~ARG[8]] <= ~ARG[9];
  end
  if (~ARG[4]) begin
    ~RESULT <= ~SYM[1][~ARG[6]];
  end~FI
end~ELSE
always @(~IF~ACTIVEEDGE[Rising][0]~THENposedge~ELSEnegedge~FI ~ARG[3]) begin : ~SYM[5]
  if (~ARG[7]) begin
    ~SYM[1][~ARG[8]] <= ~ARG[9];
  end
  ~RESULT <= ~SYM[1][~ARG[6]];
end~FI
// blockRam end"
      }
    }
  ]
  |]) #-}


fromJustX :: HasCallStack => Maybe a -> a
fromJustX Nothing  = errorX "fromJustX: Nothing"
fromJustX (Just x) = x

{-# NOINLINE blockRam# #-}
{-# ANN blockRam# hasBlackBox #-}
blockRam# 
  :: forall n dom a addr. 
    ( KnownDomain dom             --       ARG[0]
    , HasCallStack                --       ARG[1]
    , KnownNat a )                --       ARG[2]
  => Clock dom                    -- clk,  ARG[3]
  -> Enable dom                   -- en,   ARG[4]
  -> Vec n (BitVector a)          -- init, ARG[5]
  -> Signal dom (BitVector addr)  -- rd,   ARG[6]
  -> Signal dom Bool              -- wren, ARG[7]
  -> Signal dom (BitVector addr)  -- wr,   ARG[8]
  -> Signal dom (BitVector a)     -- din,  ARG[9]
  -> Signal dom (BitVector a)
blockRam# clk en init rd wren wr din
  = pure $ deepErrorX "blockRam: undefined"


{-# NOINLINE blockRam' #-}
blockRam'
  :: forall n dom addr a.
     ( KnownDomain dom
     , HasCallStack
     , BitPack a
     , KnownNat (BitSize a)
     , KnownNat addr )
  => Clock dom                    -- ^ Clock
  -> Enable dom                   -- ^ Enable
  -> Vec n a                      -- ^ Initial Ram Contents
  -> Signal dom (Unsigned addr)   -- ^ Read Address
  -> Signal dom Bool              -- ^ Write Enable
  -> Signal dom (Unsigned addr)   -- ^ Write Address
  -> Signal dom a                 -- ^ Data In
  -> Signal dom a                 -- ^ Data Out
blockRam' clk en init rd wren wr din
  = unpack <$> dout
  where dout = blockRam# clk en (pack <$> init)
                         (pack <$> rd) wren
                         (pack <$> wr) (pack <$> din)