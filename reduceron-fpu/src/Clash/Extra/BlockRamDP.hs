{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
module Clash.Extra.BlockRamDP where

import           Clash.Annotations.Primitive
import           Clash.Prelude
import           Data.Maybe             (isJust)
import           GHC.TypeNats
import           GHC.Stack              (HasCallStack, withFrozenCallStack)
import           Data.String.Interpolate      (i)
import           Data.String.Interpolate.Util (unindent)
import           Clash.XException
  (maybeIsX, seqX, NFDataX, deepErrorX, defaultSeqX, errorX)


{-# ANN blockRamDP# (InlinePrimitive Verilog $ unindent [i|
  [ { "BlackBox" :
      { "name" : "Clash.Extra.BlockRamDP.blockRamDP#"
      , "kind": "Declaration"
      , "type" :
"blockRamDP#
  :: ( KnownDomain domA --      ARG[0]
     , KnownDomain domB --      ARG[1]
     , HasCallStack   --        ARG[2]
     , KnownNat dat)  --        ARG[3]
  => Clock domA       -- clkA,  ARG[4]
  -> Enable domA      -- enA,   ARG[5]
  -> Clock domB       -- clkB,  ARG[6]
  -> Enable domB      -- enB,   ARG[7]
  -> Vec n (BitVector dat)          -- init,  ARG[8]
  -> Signal domA (BitVector addr)  -- addrA, ARG[9]
  -> Signal domA Bool -- wrenA, ARG[10]
  -> Signal domA (BitVector dat)    -- dinA,  ARG[11]
  -> Signal domB (BitVector addr)  -- addrB, ARG[12]
  -> Signal domB Bool -- wrenB, ARG[13]
  -> Signal domB (BitVector dat)    -- dinB,  ARG[14]
  -> (Signal domA (BitVector dat), Signal domB (BitVector dat))"
      , "outputReg" : true
      , "template" :
"// blockRamDP begin
reg ~TYPO ~GENSYM[~RESULT_RAM][1] [0:~LENGTH[~TYP[8]]-1];
reg ~TYP[8] ~GENSYM[ram_init][3];
integer ~GENSYM[i][4];
initial begin
  ~SYM[3] = ~CONST[8];
  for (~SYM[4]=0; ~SYM[4] < ~LENGTH[~TYP[8]]; ~SYM[4] = ~SYM[4] + 1) begin
    ~SYM[1][~LENGTH[~TYP[8]]-1-~SYM[4]] = ~SYM[3][~SYM[4]*~SIZE[~TYP[11]]+:~SIZE[~TYP[11]]];
  end
end

// Port A
~IF ~ISACTIVEENABLE[5] ~THEN
always @(~IF~ACTIVEEDGE[Rising][0]~THENposedge~ELSEnegedge~FI ~ARG[4]) begin
  ~IF ~VIVADO ~THEN
    if (~ARG[5]) begin
      ~RESULT[~SIZE[~TYP[11]]-1:0] <= ~SYM[1][~ARG[9]];
      if (~ARG[10]) begin
        ~SYM[1][~ARG[9]] <= ~ARG[11];
        ~RESULT[~SIZE[~TYP[11]]-1:0] <= ~ARG[11];
      end
    end
  end
  ~ELSE
  if (~ARG[5]) 
    ~RESULT[~SIZE[~TYP[11]]-1:0] <= ~SYM[1][~ARG[9]];
  end
  if (~ARG[10] & ~ARG[5])
    ~SYM[1][~ARG[9]] <= ~ARG[11];
    ~RESULT[~SIZE[~TYP[11]]-1:0] <= ~ARG[11];
  end
  ~FI
end
~ELSE
always @(~IF~ACTIVEEDGE[Rising][0]~THENposedge~ELSEnegedge~FI ~ARG[4]) begin
  ~RESULT[~SIZE[~TYP[11]]-1:0] <= ~SYM[1][~ARG[9]];
  if (~ARG[10]) begin
    ~SYM[1][~ARG[9]] <= ~ARG[11];
    ~RESULT[~SIZE[~TYP[11]]-1:0] <= ~ARG[11];
  end
end~FI

// Port B
~IF ~ISACTIVEENABLE[7] ~THEN
always @(~IF~ACTIVEEDGE[Rising][1]~THENposedge~ELSEnegedge~FI ~ARG[6]) begin
  ~IF ~VIVADO ~THEN
    if (~ARG[7]) begin
      ~RESULT[~SIZE[~TYP[14]]*2-1:~SIZE[~TYP[14]]] <= ~SYM[1][~ARG[12]];
      if (~ARG[13]) begin
        ~SYM[1][~ARG[12]] <= ~ARG[14];
        ~RESULT[~SIZE[~TYP[14]]*2-1:~SIZE[~TYP[14]]] <= ~ARG[14];
      end
    end
  end
  ~ELSE
  if (~ARG[7]) 
    ~RESULT[~SIZE[~TYP[14]]*2-1:~SIZE[~TYP[14]]] <= ~SYM[1][~ARG[12]];
  end
  if (~ARG[13] & ~ARG[7])
    ~SYM[1][~ARG[12]] <= ~ARG[14];
    ~RESULT[~SIZE[~TYP[14]]*2-1:~SIZE[~TYP[14]]] <= ~ARG[14];
  end
  ~FI
end
~ELSE
always @(~IF~ACTIVEEDGE[Rising][1]~THENposedge~ELSEnegedge~FI ~ARG[6]) begin
  ~RESULT[~SIZE[~TYP[14]]*2-1:~SIZE[~TYP[14]]] <= ~SYM[1][~ARG[12]];
  if (~ARG[13]) begin
    ~SYM[1][~ARG[12]] <= ~ARG[14];
    ~RESULT[~SIZE[~TYP[14]]*2-1:~SIZE[~TYP[14]]] <= ~ARG[14];
  end
end~FI
// blockRamDP end"
      }
    }
  ]
  |]) #-}


fromJustX :: HasCallStack => Maybe a -> a
fromJustX Nothing  = errorX "fromJustX: Nothing"
fromJustX (Just x) = x


blockRamDP#
  ::  forall a domA domB addr n.
     ( KnownDomain domA   --        ARG[0]
     , KnownDomain domB   --        ARG[1]
     , HasCallStack
     , KnownNat a )       --        ARG[3]
  => Clock domA           -- clkA,  ARG[4]
  -> Enable domA          -- enA,   ARG[5]
  -> Clock domB           -- clkB,  ARG[6]
  -> Enable domB          -- enB,   ARG[7]
  -> Vec n (BitVector a)           -- init,  ARG[8]
  -> Signal domA (Unsigned addr)   -- rdA,   ARG[9]
  -> Signal domA Bool              -- wrenA, ARG[10]
  -> Signal domA (BitVector a)     -- dinA,  ARG[11]
  -> Signal domB (Unsigned addr)   -- rdB,   ARG[12]
  -> Signal domB Bool              -- wrenB, ARG[13]
  -> Signal domB (BitVector a)     -- dinB,  ARG[14]
  -> (Signal domA (BitVector a)
     , Signal domB (BitVector a))
blockRamDP# clkA enA clkB enB init addrA wrenA dinA addrB wrenB dinB
  = (pure $ deepErrorX "blockRamDP: undefined", pure $ deepErrorX "blockRam: intial value undefined")
{-# NOINLINE blockRamDP# #-}
{-# ANN blockRamDP# hasBlackBox #-}

blockRamDP'
  :: forall a domA domB addr n.
     ( KnownDomain domA
     , KnownDomain domB
     , HasCallStack
     , KnownNat addr
     , BitPack a
     , KnownNat (BitSize a ))
  => Clock domA                     -- clkA,  ARG[4]
  -> Enable domA                    -- enA,   ARG[5]
  -> Clock domB                     -- clkB,  ARG[6]
  -> Enable domB                    -- enB,   ARG[7]
  -> Vec n a                        -- init,  ARG[8]
  -> Signal domA (Unsigned addr)    -- rdA,   ARG[9]
  -> Signal domA Bool               -- wrenA, ARG[10]
  -> Signal domA a                  -- dinA,  ARG[11]
  -> Signal domB (Unsigned addr)    -- rdB,   ARG[12]
  -> Signal domB Bool               -- wrenB, ARG[13]
  -> Signal domB a                  -- dinB,  ARG[14]
  -> ( Signal domA a               -- doutA
     , Signal domB a )             -- doutB
blockRamDP' clkA enA clkB enB init addrA wrenA dinA addrB wrenB dinB
  = (unpack <$> doutA, unpack <$> doutB)
  where (doutA, doutB) = blockRamDP# clkA enA clkB enB (pack <$> init)
                                     addrA wrenA (pack <$> dinA)
                                     addrB wrenB (pack <$> dinB)
{-# NOINLINE blockRamDP' #-}


blockRamDP
  :: forall a domA domB addr n.  
    ( KnownDomain domA
     , KnownDomain domB
     , HasCallStack
     , NFDataX a
     , BitPack addr
     , KnownNat (BitSize addr)
     , BitPack a
     , KnownNat (BitSize a) )
  => Clock domA
  -- ^ 'Clock' to synchronize to for port A
  -> Enable domA
  -- ^ Global enable for port A
  -> Clock domB
  -- ^ 'Clock' to synchronize to for port B
  -> Enable domB
  -- ^ Global enable for port B
  -> Vec n a
  -- ^ Initial content of the BRAM, also determines the size, @n@, of the BRAM.
  -- __NB__: __MUST__ be a constant.
  
  -> Signal domA addr
  -- ^ Read address @r@
  -> Signal domA (Maybe a)
  -- ^ (write address @w@, value to write)

  -> Signal domB addr
  -- ^ Read address @r@
  -> Signal domB (Maybe a)
  -- ^ (write address @w@, value to write)
  -> (Signal domA a, Signal domB a)
  -- ^ Values of the @blockRAM@ at address @r@ from the previous clock cycle
blockRamDP = \clkA genA clkB genB content addrA wrAM addrB wrBM ->
  let enA       = isJust <$> wrAM
      enB       = isJust <$> wrBM
      dinA = (pack . fromJustX) <$> wrAM
      dinB = (pack . fromJustX) <$> wrBM
      (doutA, doutB) = withFrozenCallStack
        (blockRamDP' clkA genA clkB genB (pack <$> content)
                    (bitCoerce <$> addrA) enA dinA
                    (bitCoerce <$> addrB) enB dinB)
  in (unpack <$> doutA, unpack <$> doutB)
{-# NOINLINE blockRamDP #-}