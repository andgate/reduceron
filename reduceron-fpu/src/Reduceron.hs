{-# LANGUAGE RecordWildCards,
             ScopedTypeVariables,
             DataKinds,
             ConstraintKinds,
             LambdaCase,
             PartialTypeSignatures #-}
module Reduceron where

import Clash.Prelude

import Data.Tuple

import Control.Monad.State
import Control.Lens

import Reduceron.Bytecode
import Reduceron.FPU

import Reduceron.CachingOctostack
import Reduceron.Code
import Reduceron.Heap
import Reduceron.Unistack


-- Modules

topEntity
  :: Clock System
  -> Reset System
  -> Enable System
  -> Signal System App
topEntity = exposeClockResetEnable system
{-# NOINLINE topEntity #-}

system
  :: HiddenClockResetEnable dom
  => Signal dom App
system = heapOut
  where
    heapOut = heap heapRead heapWrite
    heapRead = fpuOutHeapRead <$> fpuOut
    heapWrite = fpuOutHeapWrite <$> fpuOut

    stackOut = stack stackRead stackWrite
    stackRead = _memOutStackRead <$> memOut
    stackWrite = _memOutStackWrite <$> memOut

    astackOut = astack astackRead astackWrite
    astackRead = _memOutAStackRead <$> memOut
    astackWrite = _memOutAStackWrite <$> memOut

    codeOut = code codeRead (pure Nothing)
    codeRead = fpuOutTemplatesRead <$> fpuOut

    memOut = fpu memIn
    memIns = MemIn <$> stackOut <*> astackOut

stackN = d1024
stack = blockRam (replicate vstackN (0 :: Int))

astackN = d1024
astack = blockRam (replicate vstackN (0 :: Int))

heapN = d1024
heap = blockRam (replicate heapN (0 :: App))

vstackN = d1024
vstack = blockRam (replicate vstackN (0 :: Atom))

ustackN = d1024
ustack = blockRam (replicate ustackN (0 :: Update))

-- astackN = d1024
-- astack = blockRam (replicate astackN (0 :: FunAddr))

codeN = d1024
code = blockRam (replicate codeN (0 :: Template))


{-

Template and application pre-fetching
-------------------------------------

The fetch function determines the location of the needed template in
code memory.  The fetchApp function pre-fetches an application from
the heap, in preperation for the unwinding operation.

-}

-- fetch :: Atom -> FunAddr -> FunAddr
-- fetch top alts = isCON top ? (jump, funAddr top)
--   where jump = alts + conIndex top

-- fetchApp :: Reduceron -> Recipe
-- fetchApp r = iff cond $ r.heap.lookupB (r.newTop.val.pointer)
--   where cond = isUnwindState (r.state)
--            <|> isSwapState (r.state)
--            <|> (isUnfoldState (r.state) <&> inv (r.code.templateInstAtoms2))


-- testBench :: Signal System Bool
-- testBench = done
--   where
--     testInput    = stimuliGenerator clk rst $(listToVecTH [(1,1) :: (Signed 9,Signed 9),(2,2),(3,3),(4,4)])
--     expectOutput = outputVerifier clk rst $(listToVecTH [1 :: Signed 9,1,5,14])
--     done         = expectOutput (topEntity clk rst testInput)
--     clk          = tbSystemClockGen (not <$> done)
--     rst          = systemResetGen