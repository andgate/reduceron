let
  inherit (import <nixpkgs> {}) fetchFromGitHub;

	clash-platform = import (fetchFromGitHub {
    # Descriptive name to make the store path easier to identify
		owner = "clash-lang";
		repo = "clash-compiler";
		# Commit hash for 1.0 as of 11-10-2019
		# `git ls-remote https://github.com/clash-lang/clash-compiler/ 1.0`
		rev = "74af8d85dce252186235ba9a857c53cd953ec88c";
		# Fetched with 
		# `nix-prefetch-url --unpack https://github.com/clash-lang/clash-compiler/archive/1.0.tar.gz`
		sha256 = "1jm342v32q2vg23pldhbjshhhx3krmrddgi546yiw9yawl83acf1";
	  }) {};

  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: rec {
            clash-ghc = clash-platform.clash-ghc;
            clash-lib = clash-platform.clash-lib;
            clash-prelude = clash-platform.clash-prelude;
        };
      };
    };
  };

  # nixpkgs1909 = import (fetchFromGitHub {
  #   # Descriptive name to make the store path easier to identify
  #     owner = "NixOS";
  #     repo = "nixpkgs";
  #     # Commit hash for nixos-19.09 as of 11-10-2019
  #     # `git ls-remote https://github.com/NixOS/nixpkgs-channels/ nixos-19.09`
  #     rev = "5d755e916d567561a7771aa6d8fc81a27ab17cfb";
  #     # Fetched with 
  #     # `nix-prefetch-url --unpack https://github.com/nixos/nixpkgs-channels/archive/nixos-19.09.tar.gz`
  #     sha256 = "1vj3bwljkh55si4qjx52zgw7nfy6mnf324xf1l2i5qffxlh7qxb6";
  #   }) {};

  nixpkgs1903 = import (fetchFromGitHub {
    # Descriptive name to make the store path easier to identify
      owner = "NixOS";
      repo = "nixpkgs";
      # Commit hash for nixos-19.09 as of 11-10-2019
      # `git ls-remote https://github.com/NixOS/nixpkgs/ release-19.03`
      rev = "6c08aa0d57e12209d199445a9fe569875f9d5887";
      # Fetched with 
      # `nix-prefetch-url --unpack https://github.com/nixos/nixpkgs/archive/release-19.03.tar.gz`
      sha256 = "0gxcm9q87jvimw26pji9blizxpb1mq86c9amz19kcix2pjj1xwsr";
  }) { inherit config; };

  nixpkgs = nixpkgs1903;
  pkgs = nixpkgs.pkgs;
in
nixpkgs.stdenv.mkDerivation {
  name = "reduceron-fpu";
  src = ./.;

  buildInputs = with pkgs; [
    (pkgs.haskellPackages.ghcWithPackages (p: with p; [
      clash-ghc
      clash-prelude
      interpolate

      ghc-typelits-extra
      ghc-typelits-knownnat
      ghc-typelits-natnormalise
    ])
    )
	];

  buildPhase = ''
		# Build FPU Spec
		clash -isrc --verilog src/Reduceron.hs --verilog -ddump-simpl

		# Build Simulator
		clash -isrc src/Main.hs -o reduceron

		# # Generate testBench for vvp
		# iverilog -Wall -s Sky_testBench -o Sky_testBench \
		#         verilog/Sky/Sky_testBench/Sky_testBench.v \
		#         verilog/Sky/Sky_testBench/Sky_outputVerifier.v \
		#         verilog/Sky/Sky_testBench/Sky_stimuliGenerator.v \
		#         verilog/Sky/Sky_topEntity.v
  '';

  doCheck = true;
  checkPhase = ''
    # vvp Sky_testBench
  '';

  installPhase = ''
    mkdir -p $out
    cp -r verilog $out
    cp -r reduceron $out
    # cp -r Sky_testBench $out
  '';
}