{ pkgs ? import <nixpkgs> {} }:
pkgs.stdenv.mkDerivation {
  name = "Demo-ice40-hx8k-ct256";
  src= ./.;

  buildInputs = with pkgs; [
    yosys
    arachne-pnr
    icestorm
  ];

  buildPhase = ''
    yosys -v3 -l synth.log -p "synth_ice40 -blif blink.blif" top.v pll.v
    arachne-pnr -d 8k -P ct256 -p pins.pcf blink.blif -o blink.asc
    icetime -tmd hx8k blink.asc
    icepack blink.asc blink.bin
  '';

  installPhase = ''
    mkdir -p $out
    cp blink.bin $out/
    # Check if ice is connected and flash?
    # iceprog blink.bin
  '';
}