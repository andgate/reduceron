let
  inherit (import <nixpkgs> {}) fetchFromGitHub;

  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: rec {
            language-t = pkgs.haskell.lib.dontHaddock 
              (self.callCabal2nix "language-t" ./language-t {});
            language-g = pkgs.haskell.lib.dontHaddock
              (self.callCabal2nix "language-g" ./language-g { language-t = language-t; });
        };
      };
    };
  };

  nixpkgs1909 = import (fetchFromGitHub {
    # Descriptive name to make the store path easier to identify
      owner = "NixOS";
      repo = "nixpkgs";
      # Commit hash for nixos-19.09 as of 11-10-2019
      # `git ls-remote https://github.com/NixOS/nixpkgs-channels/ nixos-19.09`
      rev = "5d755e916d567561a7771aa6d8fc81a27ab17cfb";
      # Fetched with 
      # `nix-prefetch-url --unpack https://github.com/nixos/nixpkgs-channels/archive/nixos-19.09.tar.gz`
      sha256 = "1vj3bwljkh55si4qjx52zgw7nfy6mnf324xf1l2i5qffxlh7qxb6";
    }) { inherit config; };

  nixpkgs = nixpkgs1909;
  pkgs = nixpkgs.pkgs;

  language-g = pkgs.haskellPackages.language-g;
in
pkgs.runCommand "golden" {
  src = ./language-g;
  buildInputs = [ pkgs.cabal-install pkgs.ghc language-g ];
} ''
  mkdir -p $out/golden/
  cp -r $src/golden/* $out/golden/
  cd $out
  g-golden || true
''
