module Language.T.Interpret where

import Language.T.Syntax

-- Interpreter State
type State = (Prog, Heap, Stack, UStack)


-- Heap (Graph memory)
type Heap = [App]
type HeapAddr = Int

update :: HeapAddr -> App -> Heap -> Heap
update i a as = take i as ++ [a] ++ drop (i+1) as


-- Stack (Template memory)
type Stack = [Atom]
type StackAddr = Int

-- Update Stack
type UStack = [(StackAddr, HeapAddr)]


-- Interpret
interpret :: Prog -> Int
interpret p = eval (p, [], [AFun 0 0], [])

-- Evaluator runs steps until termination
eval :: State -> Int
eval (p, h, [AInt i], u) = i
eval s = eval (step s)


-- One reduction step for each atom on the stack
step :: State -> State

-- Unwinding
step (p, h, APtr x:s, u) = (p, h, h!!x ++ s, upd:u)
  where upd = (1+length s, x)

-- Updating
step (p, h, top:s, (sa, ha):u)
  | arity top > n = (p, h', top:s, u)
  where
    n = 1+length s - sa
    h' = update ha (top:take n s) h


-- Integers and Primitives
step (p, h, AInt n:x:s, u) = (p, h, x:AInt n:s, u)
step (p, h, APrim f:x:y:s, u) = (p, h, prim f x y:s, u)


-- Constructors
step (p, h, ACon n j:s, u) = (p, h, AFun 0 (i+j):s, u)
  where ATab i = s!!n

-- Function Application
step (p, h, AFun n f:s, u) = (p, h', s', u)
  where
    (arity, spine, apps) = p!!f
    h' = h ++ map (instApp s h) apps
    s' = instApp s h spine ++ drop arity s

-- Instantiation 
instApp :: Stack -> Heap -> App -> App
instApp s h = map (inst s (length h))

inst :: Stack -> HeapAddr -> Atom -> Atom
inst s base (APtr p) = APtr (base + p)
inst s base (AArg i) = s!!i
inst s base a = a

-- Intepreter Helpers
prim :: String -> Atom -> Atom -> Atom
prim "(+)" (AInt n) (AInt m) = AInt (n+m)
prim "(-)" (AInt n) (AInt m) = AInt (n+m)
prim "(<=)" (AInt n) (AInt m) = bool (n<=m)

bool :: Bool -> Atom
bool False = ACon 0 0
bool True = ACon 0 1

arity :: Atom -> Arity
arity (AFun n i) = n
arity (AInt i) = 1
arity (ACon n i) = n+1
arity (APrim p) = 2