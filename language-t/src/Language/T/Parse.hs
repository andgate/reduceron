module Language.T.Parse where

import Control.Monad (void)
import Control.Monad.Combinators.Expr -- from parser-combinators
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Language.T.Syntax


type Parser = Parsec Void String

sc :: Parser ()
sc = L.space space1 lineCmnt blockCmnt
  where
    lineCmnt  = L.skipLineComment "//"
    blockCmnt = L.skipBlockComment "/*" "*/"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

brackets :: Parser a -> Parser a
brackets = between (symbol "[") (symbol "]")

integer :: Parser Integer
integer = lexeme L.decimal

intP :: Parser Int
intP = fromInteger <$> integer

-- Reserverd words
rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

-- List of reserved words
rws :: [String] -- list of reserved words
rws = ["FUN","ARG","PTR","CON","INT","PRIM","TAB"]


identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
  where
    p       = (:) <$> letterChar <*> many alphaNumChar
    check x = if x `elem` rws
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else return x


-- TODO: Parse program


progP :: Parser Prog
progP = many templateP

templateP :: Parser Template
templateP = parens $
  (,,) <$> intP <*> appP <*> appListP

appListP :: Parser [App]
appListP = many appP

appP :: Parser App
appP = undefined -- brackets (commaSep atomP)

atomP :: Parser Atom
atomP = atomFunP
    <|> atomArgP
    <|> atomPtrP
    <|> atomConP
    <|> atomIntP
    <|> atomPrimP
    <|> atomTabP

atomFunP :: Parser Atom
atomFunP = AFun <$> (rword "FUN" *> intP) <*> intP

atomArgP :: Parser Atom
atomArgP = AArg <$> (rword "ARG" *> intP)

atomPtrP :: Parser Atom
atomPtrP = APtr <$> (rword "PTR" *> intP)

atomConP :: Parser Atom
atomConP = ACon <$> (rword "CON" *> intP) <*> intP

atomIntP :: Parser Atom
atomIntP = AInt <$> (rword "INT" *> intP)

atomPrimP :: Parser Atom
atomPrimP = APrim <$> (rword "PRI" *> undefined)

atomTabP :: Parser Atom
atomTabP = ATab <$> (rword "TAB" *> intP)