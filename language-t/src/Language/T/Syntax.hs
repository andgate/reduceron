module Language.T.Syntax where

-- Syntax
type Prog = [Template]

type Template = (Arity, App, [App])
type Arity = Int

type App = [Atom]

data Atom
  = AFun Arity Int    -- Function with arity and address
  | AArg Int          -- Reference to a function argument
  | APtr Int          -- Pointer to an application
  | ACon Arity Int    -- Constructor with arity and index
  | AInt Int          -- Integer Literal
  | APrim String      -- Primitive Name
  | ATab Int          -- Case table