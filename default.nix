{ nixpkgs ? import ./nix/nixpkgs.nix {} }:

with nixpkgs.pkgs.haskellPackages;
let
  language-g = import ./language-g {};
  language-t = import ./language-t {};
  reduceron-fpu = import ./reduceron-fpu {};
in
  { inherit language-g language-t reduceron-fpu; }