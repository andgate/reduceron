{-# LANGUAGE RecordWildCards,
             ScopedTypeVariables,
             DataKinds,
             ConstraintKinds,
             LambdaCase,
             PartialTypeSignatures #-}
module MAC where

import Clash.Prelude

ma acc (x,y) = acc + (resize (x * y))

macT acc (x,y) = (acc',o)
  where
    acc' = ma acc (x,y)
    o    = acc

mac = mealy macT 0


{-# ANN topEntity
  (Synthesize
    { t_name   = "MAC"
    , t_inputs = [ PortName "Clock"
                 , PortName "Reset"
                 , PortName "A"
                 , PortName "B"
                 ]
    , t_output = PortName "C"
    }) #-}
topEntity
  :: Clock System
  -> Reset System
  -> Signal System (Unsigned 16)
  -> Signal System (Unsigned 16)
  -> Signal System (Unsigned 32)
topEntity clk rst a b =
  exposeClockResetEnable mac clk rst en (bundle (a, b))
  where
    en = enableGen