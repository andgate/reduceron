-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.mac_types.all;

entity MAC is
  port(-- clock
       Clock : in mac_types.clk_system;
       -- reset
       Reset : in mac_types.rst_system;
       A     : in unsigned(15 downto 0);
       B     : in unsigned(15 downto 0);
       C     : out unsigned(31 downto 0));
end;

architecture structural of MAC is
  -- MAC.hs:37:1-9
  signal \c$acc_rec\ : unsigned(31 downto 0);

begin
  -- register begin
  mac_register : block
    signal cacc_rec_reg : unsigned(31 downto 0) := to_unsigned(0,32);
  begin
    \c$acc_rec\ <= cacc_rec_reg; 
    cacc_rec_r : process(Clock,Reset)
    begin
      if Reset =  '1'  then
        cacc_rec_reg <= to_unsigned(0,32)
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(Clock) then
        cacc_rec_reg <= (\c$acc_rec\ + (resize((resize(A * B, 16)),32)))
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  C <= \c$acc_rec\;


end;

